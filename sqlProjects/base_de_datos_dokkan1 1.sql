create database dokkanbattle;
use dokkanbattle;
SET SQL_SAFE_UPDATES = 0;


-- Un personaje tiene un nombre,una habilidad,una categoría, determinados link skills,pasiva,efecto de pasiva,active skill
-- Un personaje puede tener una o más habilidades, y una habilidad puede pertenecer a varios personajes
-- Un personaje tiene solo un tipo
-- Un personaje puede tener más de una categoría, y una categoría puede pertenecer a varios personajes.
-- Un personaje puede tener más de una link skill, y una link skill puede pertenecer a varios personajes.
-- Un personaje puede participar en un solo evento a la vez, y en un evento pueden participar uno o  más personajes.




create table tability(
	Idability int not null primary key,
    name varchar(255) not null,
    effect varchar(255) not null
);

create table tcategory(
	Idcategory int not null primary key,
    name varchar(255) not null
);

create table ttype(
	Idtype int not null primary key,
    type varchar(255) not null

);

-- Para tener una referencia en el futuro, estos linkskills son al lvl 10
create table tlinkskill(
	Idlink int not null primary key,
    link varchar(255) not null,
    effect_of_link varchar(255) not null
);

create table tevent(
	Idevent int not null primary key,
    name_of_event varchar(255) not null
);


create table tcharacter(
	Idcharacter int not null primary key,
    name varchar(255) not null,
    Idtype int not null,
    Idevent int not null,
    constraint fk_type_character foreign key(Idtype) references ttype(Idtype),
    constraint fk_event_character foreign key(Idevent) references tevent(Idevent),
    passiveskill varchar(255) not null
);

create table tcharacter_tcategory(
	Idcharacter int not null,
    constraint fk_charactercategory foreign key(Idcharacter) references tcharacter(Idcharacter),
    Idcategory int not null,
    constraint fk_categorycharacter foreign key(Idcategory) references tcategory(Idcategory),
    primary key(Idcharacter,Idcategory)
);


create table tcharacter_tlinkskill(
	Idcharacter int not null,
    constraint fk_characterlink foreign key(Idcharacter) references tcharacter(Idcharacter),
    Idlink int not null,
    constraint fk_linkcharacter foreign key(Idlink) references tlinkskill(Idlink),
    primary key(Idcharacter,Idlink)
);


create table tcharacter_tability(
	Idcharacter int not null,
    Idability int not null,
    constraint fk_characterability foreign key(Idcharacter) references tcharacter(Idcharacter),
    constraint fk_abilitycategory foreign key(Idability) references tability(Idability),
    primary key(Idcharacter,Idability)

);







/*('Fusion','Shadow Dragon Saga','World Tournament','Peppy Gals',
    'Hybrid Saiyans', 'Universe Survival Saga','Resurrected Warriors','Realm of Gods','Majin Buu Saga','Potara',
    'Low-Class Warrior','Super Saiyan 3','Giant Form','Planet Namek Saga','Ginyu Force','Movie Bosses','Pure Saiyans',
    'Namekians','Future Saga','Full Power','Androids','Representatives of Universe 7','Transformation Boost','Wicked Bloodline',
    'Dragon Ball Seekers','Time Travelers','Universe 6','Joined Forces','Movie Heroes','Goku Family','Vegeta Family','Artificial Life Forms',
    'Youth','DB Saga','Siblings Bond','Super Saiyans','Worthy Rivals','Androids/Cell Saga','Kamehameha','Bond of Master and Disciple',
    'Terrifying Conquerors','Dragon Ball Heroes','Target:Goku','Otherworld Warriors','Super Saiyan 2','Final Trump Card','Exploding Rage',
    'Revenge','Team Bardock','Inhumam Deeds','Earthlings','Special Pose','Majin Power','Rapid Growth','All-Out Struggle','Universe 11','Saviors',
    'Battle of Wits','Power Absorption','Giant Ape Power','Crossover','Space-Traveling Warriors','Connected Hope','Corroded Body and Mind',
    'Turtle School','Miraculous Awakening','Powerful Comeback','Gifted Warriors','Planetary Destruction','Defenders of Justice',
    'Storied Figures','GT Heroes','GT Bosses','Heavenly Events','Time Limit','Legendary Existence','Sworn Enemies','Accelerated Battle')),*/
    
    
-- INSERTS

-- INICIO insert INTO tcategory

insert into tcategory values (01,'Fusion');
insert into tcategory values (02,'Shadow Dragon Saga');
insert into tcategory values (03,'World Tournament');
insert into tcategory values (04,'Peppy Gals');
insert into tcategory values (05,'Hybrid Saiyans');
insert into tcategory values (06,'Universe Survival Saga');
insert into tcategory values (07,'Resurrected Warriors');
insert into tcategory values (08,'Realm of Gods');
insert into tcategory values (09,'Majin Buu Saga');
insert into tcategory values (010,'Potara');
insert into tcategory values (011,'Low-Class Warrior');
insert into tcategory values (012,'Super Saiyan 3');
insert into tcategory values (013,'Giant Form');
insert into tcategory values (014,'Planet Namek Saga');
insert into tcategory values (015,'Ginyu Force');
insert into tcategory values (016,'Movie Bosses');
insert into tcategory values (017,'Pure Saiyans');
insert into tcategory values (018,'Namekians');
insert into tcategory values (019,'Future Saga');
insert into tcategory values (020,'Full Power');
insert into tcategory values (021,'Androids');
insert into tcategory values (022,'Representatives of Universe 7');
insert into tcategory values (023,'Transformation Boost');
insert into tcategory values (024,'Wicked Bloodline');
insert into tcategory values (025,'Dragon Ball Seekers');
insert into tcategory values (026,'Time Travelers');
insert into tcategory values (027,'Universe 6');
insert into tcategory values (028,'Joined Forces');
insert into tcategory values (029,'Movie Heroes');
insert into tcategory values (030,'Goku Family');
insert into tcategory values (031,'Vegeta Family');
insert into tcategory values (032,'Artificial Life Forms');
insert into tcategory values (033,'Youth');
insert into tcategory values (034,'DB Saga');
insert into tcategory values (035,'Siblings Bond');
insert into tcategory values (036,'Super Saiyans');
insert into tcategory values (037,'Worthy Rivals');
insert into tcategory values (038,'Androids/Cell Saga');
insert into tcategory values (039,'Kamehameha');
insert into tcategory values (040,'Bond of Master and Disciple');
insert into tcategory values (041,'Terrifying Conquerors');
insert into tcategory values (042,'Dragon Ball Heroes');
insert into tcategory values (043,'Target:Goku');
insert into tcategory values (044,'Otherworld Warriors');
insert into tcategory values (045,'Super Saiyan 2');
insert into tcategory values (046,'Final Trump Card');
insert into tcategory values (047,'Exploding Rage');
insert into tcategory values (048,'Revenge');
insert into tcategory values (049,'Team Bardock');
insert into tcategory values (050,'Inhumam Deeds');
insert into tcategory values (051,'Earthlings');
insert into tcategory values (052,'Special Pose');
insert into tcategory values (053,'Majin Power');
insert into tcategory values (054,'Rapid Growth');
insert into tcategory values (055,'All-Out Struggle');
insert into tcategory values (056,'Universe 11');
insert into tcategory values (057,'Saviors');
insert into tcategory values (058,'Battle of Wits');
insert into tcategory values (059,'Power Absorption');
insert into tcategory values (060,'Giant Ape Power');
insert into tcategory values (061,'Crossover');
insert into tcategory values (062,'Space-Traveling Warriors');
insert into tcategory values (063,'Connected Hope');
insert into tcategory values (064,'Corroded Body and Mind');
insert into tcategory values (065,'Turtle School');
insert into tcategory values (066,'Miraculous Awakening');
insert into tcategory values (067,'Powerful Comeback');
insert into tcategory values (068,'Gifted Warriors');
insert into tcategory values (069,'Planetary Destruction');
insert into tcategory values (070,'Defenders of Justice');
insert into tcategory values (071,'Storied Figures');
insert into tcategory values (072,'GT Heroes');
insert into tcategory values (073,'GT Bosses');
insert into tcategory values (074,'Heavenly Events');
insert into tcategory values (075,'Time Limit');
insert into tcategory values (076,'Legendary Existence');
insert into tcategory values (077,'Sworn Enemies');
insert into tcategory values (078,'Accelerated Battle');

-- FIN insert INTO tcategory

-- INICIO insert INTO ttype
insert into ttype values(01,'Super PHY');
insert into ttype values(02,'Extreme PHY');
insert into ttype values(03,'Super INT');
insert into ttype values(04,'Extreme INT');
insert into ttype values(05,'Super TEQ');
insert into ttype values(06,'Extreme TEQ');
insert into ttype values(07,'Super AGL');
insert into ttype values(08,'Extreme AGL');
insert into ttype values(09,'Super STR');
insert into ttype values(010,'Extreme STR');

-- FIN insert INTO ttype

-- INICIO insert INTO tability


insert into tability values(01,"Kamehameha","Raises ATK & DEF and causes immense damage to enemy");
insert into tability values(02,"Galick Gun","Raises DEF and causes immense damage to enemy");
insert into tability values(03,"Death Beam","Causes supreme damage to enemy and raises Extreme Class allies' ATK by 30% for 1 turn");
insert into tability values(04,"Masenko","Causes supreme damage to enemy and raises ATK & DEF by 30% for 9 turns");
insert into tability values(05,"Destructo-Disc","Causes huge damage to enemy and lowers DEF");
insert into tability values(06,"Special Beam Cannon","Causes huge damage to enemy and lowers DEF");
insert into tability values(07,"Tri-Beam","Causes extreme damage to enemy");
insert into tability values(08,"Dodon Ray","Extreme damage, rare chance to stun enemy");
insert into tability values(09,"Wolf Fang Fist","Causes supreme damage to enemy and lowers DEF");
insert into tability values(010,"Double Sunday","Causes supreme damage to all enemies and lowers DEF");
insert into tability values(011,"Giant Storm","Causes supreme damage to enemy");
insert into tability values(012,"Elegant Blaster","Causes supreme damage to enemy");
insert into tability values(013,"Milky Cannon","Raises ATK and causes supreme damage to enemy");
insert into tability values(014,"Eye Beam","Causes huge damage to enemy");
insert into tability values(015,"Photon Wave","Causes immense damage to enemy and recovers 10% HP");
insert into tability values(016,"Full Power Energy Ball","Causes huge damage to enemy");
insert into tability values(017,"Infinity Bullet","Causes extreme damage to enemy");
insert into tability values(018,"Rocket Punch","Causes supreme damage to enemy and massively lowers DEF");
insert into tability values(019,"Evil Containment Wave","Causes supreme damage to enemy and seals Super Attack");
insert into tability values(020,"Super Breath Cannon","Causes destructive damage to enemy");
insert into tability values(021,"Blue Impulse","Raises DEF and causes supreme damage to enemy");
insert into tability values(022,"Crusher Ball","Causes supreme damage to enemy and raises allies' ATK by 20% for 1 turn");
insert into tability values(023,"Dynamite Kick","Causes huge damage to enemy");
insert into tability values(024,"Eagle Kick","Causes supreme damage to enemy and raises ATK for 3 turns");
insert into tability values(025,"Crash Launcher","Causes huge damage to enemy");
insert into tability values(026,"Victory Cannon","Causes huge damage to enemy");
insert into tability values(027,"Super Kamehameha","Raises ATK and causes mega-colossal damage to enemy");
insert into tability values(028,"Super Galick Gun","Raises ATK and causes mega-colossal damage to enemy");
insert into tability values(029,"Final Flash","Raises DEF and causes immense damage to enemy");
insert into tability values(030,"20x Kaioken Kamehameha","Greatly raises ATK for 1 turn and causes supreme damage to enemy");
insert into tability values(031,"Grand Explosion","Causes immense damage to enemy and lowers ATK & DEF");

-- FIN INSERT INTO TABILITY

-- INICIO INSERT INTO TEVENT
insert into tevent values(01,"Warrior of fury! Super Saiyan");
insert into tevent values(02,"Young Warrior's Resolve");
insert into tevent values(03,"Unified as a Super Namekian");
insert into tevent values(04,"Earth-Shaking Showdown(vs. Goku)");
insert into tevent values(05,"Surpassing Even the Gods");
insert into tevent values(06,"Earth-Shaking Showdown(vs. Vegeta)");
insert into tevent values(07,"Proud Prince of the Saiyans");
insert into tevent values(08,"Beacon of Hope in the War-Stricken Future");
insert into tevent values(09,"Warrior Returned from Otherworld");
insert into tevent values(010,"Defender of Beliefs and Dreams");
insert into tevent values(011,"Sinister Destroyer of the Universe");
insert into tevent values(012,"Earth-Shaking Showdown (vs. Frieza)");
insert into tevent values(013,"Ultimate Android, Incarnation of Ambitions");
insert into tevent values(014,"Phantom Majin of Smoke and Flame");
insert into tevent values(015,"Battle-Hardened Saiyan Power Explodes!");
insert into tevent values(016,"Super Awakening in the Gap of History");
insert into tevent values(017,"Battle-Hardened Saiyan Power Explodes!");
insert into tevent values(018,"The invincible");
insert into tevent values(019,"Split-Second Assasin");
insert into tevent values(020,"The Greatest Adversary of all");
insert into tevent values(021,"The Darnkness Shrouding");
insert into tevent values(022,"Penetrate! Full-Throttle Throwdown");
insert into tevent values(023,"Ultimate Android");

-- INICIO INSERT INTO TCHARACTER

-- values(Id,Name,IdType,Idevent,passive)

insert into tcharacter values(01,"Goku",07,01,"ATK +2500 when performing a Super Attack");
insert into tcharacter values(02,"Vegeta",01,07,"ATK & DEF +70%; plus an additional ATK +5% per Ki Sphere obtained");
insert into tcharacter values(03,"Krillin",05,010,"High chance of ATK +20% and Ki +2 for TEQ Type");
insert into tcharacter values(04,"Piccolo",07,03,"ATK & DEF +50%; ATK +10% and Ki +1 in addition per Rainbow Ki Sphere obtained; Assimilate when conditions are met");
insert into tcharacter values(05,"Tien",01,08,"Ki +3 and ATK & DEF +150%; plus an additional ATK +70% when performing a Super Attack; Majin Buu Saga Category Super Class allies' ATK +30% and DEF +50%; high chance of stunning the attacked enemy");
insert into tcharacter values(06,"Yamcha",09,018,"Ki +3; great chance of ATK +77%");
insert into tcharacter values(07,"Super Saiyan God SS Vegeta",05,06,"ATK & DEF +70% when facing only 1 enemy");
insert into tcharacter values(08,"Goku(Ultra Instinct)",07,016,"ATK & DEF +150%; great chance of evading enemy's attack (including Super Attack); Ki +1 (up to +5) with each attack evaded (including Super Attack); plus an additional DEF +30% within the same turn after evading an attack");
insert into tcharacter values(09,"Gogeta",09,018,"ATK & DEF +70%; Ki +3 and launches an additional attack that has a medium chance of becoming a Super Attack when attacking; high chance of evading enemy's attack (including Super Attack) as the 3rd attacker in a turn; Transforms when conditions are met");
insert into tcharacter values(010,"Android #13",08,021,"ATK & DEF +130%; Androids Category allies' Ki +2 and ATK & DEF +30%; attacks guaranteed to hit; Transforms when conditions are met");
insert into tcharacter values(011,"Android #18",08,023,"ATK & DEF +100%; chance of evading enemy's attack (including Super Attack) +18%; chance of performing a critical hit +18%");
insert into tcharacter values(012,"Android #17",01,023,"ATK +120% when performing a Super Attack; all allies' Ki +3 and DEF +60%; reduces damage received by 40% when HP is 77% or below");
insert into tcharacter values(013,"Super #17",06,021,"ATK & DEF +170%; plus an additional ATK & DEF +17% per Androids Category ally (self excluded) attacking in the same turn");

-- FIN INSERT INTO TCHARACTER

-- INICIO INSERT INTO TLINKSKILL

insert into tlinkskill values(01,"Supreme Warrior","Ki +2 and ATK +10%");
insert into tlinkskill values(02,"Saiyan Warrior Race","ATK +10%");
insert into tlinkskill values(03,"Prodigies","ATK +15%");
insert into tlinkskill values(04,"Super Saiyan","ATK +15%");
insert into tlinkskill values(05,"Experienced Fighters","ATK +15%");
insert into tlinkskill values(06,"Coward","Ki +2 and chance of performing a critical hit +5%");
insert into tlinkskill values(07,"The Saiyan Lineage","Ki +2 and ATK & DEF +5%");
insert into tlinkskill values(08,"Android Assault","Ki +2 and DEF +20%");
insert into tlinkskill values(09,"Cold Judgment","DEF +25%");
insert into tlinkskill values(010,"Kamehameha","ATK +10% when performing a Super Attack");
insert into tlinkskill values(011,"Shocking Speed","Ki +2 and DEF +5%");
insert into tlinkskill values(012,"Saiyan Pride","ATK +20%");
insert into tlinkskill values(013,"Warrior Gods","ATK +10%; plus an additional ATK +5% when performing a Super Attack");

-- FIN INSERT INTO TLINKSKILL

-- INICIO INSERT INTO TCHARACTER_TCATEGORY

insert into tcharacter_tcategory values(01,017);
insert into tcharacter_tcategory values(02,017);
insert into tcharacter_tcategory values(03,051);
insert into tcharacter_tcategory values(04,018);
insert into tcharacter_tcategory values(05,051);
insert into tcharacter_tcategory values(06,051);
insert into tcharacter_tcategory values(07,08);
insert into tcharacter_tcategory values(08,08);
insert into tcharacter_tcategory values(09,01);
insert into tcharacter_tcategory values(010,021);
insert into tcharacter_tcategory values(011,038);
insert into tcharacter_tcategory values(012,021);
insert into tcharacter_tcategory values(013,021);

-- FIN INSERT INTO TCHARACTER_TCATEGORY

-- INICIO INSERT INTO TCHARACTER_TLINKSKILL

insert into tcharacter_tlinkskill values(01,07);
insert into tcharacter_tlinkskill values(02,07);
insert into tcharacter_tlinkskill values(03,03);
insert into tcharacter_tlinkskill values(04,05);
insert into tcharacter_tlinkskill values(05,05);
insert into tcharacter_tlinkskill values(06,06);
insert into tcharacter_tlinkskill values(07,013);
insert into tcharacter_tlinkskill values(08,010);
insert into tcharacter_tlinkskill values(09,07);
insert into tcharacter_tlinkskill values(010,09);
insert into tcharacter_tlinkskill values(011,09);
insert into tcharacter_tlinkskill values(012,08);
insert into tcharacter_tlinkskill values(013,08);

-- FIN INSERT INTO TCHARACTER_TLINKSKILL

-- INICIO INSERT INTO TCHARACTER_TABILITY

insert into tcharacter_tability values (01,01);
insert into tcharacter_tability values (02,02);
insert into tcharacter_tability values (03,05);
insert into tcharacter_tability values (04,06);
insert into tcharacter_tability values (05,07);
insert into tcharacter_tability values (06,01);
insert into tcharacter_tability values (07,028);
insert into tcharacter_tability values (08,027);
insert into tcharacter_tability values (09,027);
insert into tcharacter_tability values (010,015);
insert into tcharacter_tability values (011,017);
insert into tcharacter_tability values (012,017);
insert into tcharacter_tability values (013,016);

-- FIN INSERT INTO TCHARACTER_TABILITY

-- INICIO CONSULTAS

select * from tability;
select name,passiveskill from tcharacter;
select name,Idtype from tcharacter order by name asc;
select name,passiveskill from tcharacter where passiveskill like '%Transforms%';


select Idtype from tcharacter where name like 'Piccolo';
update tcharacter set Idtype = 05 where name like 'Piccolo';

select Idability,name from tability where effect like 'Raises DEF%' order by Idability asc;
select * from tlinkskill where effect_of_link like '%+10%';
select link from tlinkskill where link like '%Saiyan%';
update tability set effect = 'Causes supreme damage' where Idability between 010 and 014;
select * from tcharacter_tability order by Idcharacter,Idability asc;
select * from ttype where type like '%Super%';
select * from tability where effect like '%Raises ATK%' or '%Raises DEF%';
select name_of_event from tevent where Idevent between 01 and 05;
select Idcharacter,Idtype,passiveskill from tcharacter where name like 'Goku%';
