package terminal_final;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;



class Posicion {
	private Path posicion;
	 /**
     * @return Devuelve la variable posicion, que es la ruta en la que se encuentre en ese momento
     * @author Qiang Zhan
     */
	public Path getEstoy() {
		return posicion;
	}
/**
 * La variable posicion se iguala a la posicion en la que se está en ese momento con un Getter de la posición
 * @param posicion
 * @author Qiang Zhan
 */
	public void setEstoy(Path posicion) {
		this.posicion = posicion;
	}
}

public class terminalfinal {
	static Posicion posicionActual = new Posicion();
	private static String rutaBase = System.getProperty("user.dir");
	private static Scanner sc = new Scanner(System.in);

	/**
	 * En este main se hace un llamamiento al método correspondiente.
	 * 
	 * @param args
	 * @throws Exception
	 */
	
	public static void main(String[] args) throws Exception {
		// El usuario decide que fichero quiere mostrar, por lo que introducirá una ruta
		// absoluta
		// EJ:C:\Users\SERGI\OneDrive\Escritorio\prueba.txt
		System.out.println("Terminal Personalizada [Version 1.0.2]" +  "\nTodos los derechos reservados®" + "\n");
		boolean salida = true;
		Path rutaH = Paths.get(rutaBase);
		posicionActual.setEstoy(rutaH);
		while (salida) {
			System.out.print(posicionActual.getEstoy() + ">");
			String eleccionUsuario;
		
			eleccionUsuario = sc.nextLine();
			
			switch (eleccionUsuario) {
			case "cat":
				cat(posicionActual.getEstoy().toString());
				break;

			case "top":
				
				int nLineas = 0;

				top(nLineas, posicionActual.getEstoy().toString());
				break;

			case "cd ..":
				directorioPadre();
				break;

			case "cd":
				System.out.println(posicionActual.getEstoy().toString());
				System.out.println("");
				break;
			//Este cd va con un espacio, es decir, es un cd mas un caracter		
			case "cd ":
				System.out.println("¿A que directorio quieres acceder?");
				String otraEleccion = sc.next();
				Path prueba = Paths.get(otraEleccion);
				if (prueba.isAbsolute()) {
					rutaAbsoluta(otraEleccion);
				} else {
					entradaArchivo(otraEleccion);
				}
				break;

			case "delete":
				System.out.println("¿Estas seguro de borrar el directorio? [Si] , [No]");
				String eleccionDelete = sc.next();
				if(eleccionDelete.equals("Si")) {
					delete(posicionActual.getEstoy());
					break;
				}else {
					System.out.println("No se borrara nada");
					break;
				}
	
			case "salida":
				System.out.println("---------------------------------------------------------");
				salida = false;
				break;

			case "info":
				info(posicionActual.getEstoy());
				break;

			case "mkfile":
				mkfile(posicionActual.getEstoy());
				break;

			case "clear":
				clear();
				break;
				
			case "dirRuta":
				dirRuta();
				break;
			case "dir":
				dir(posicionActual.getEstoy().toString());
				break;
			case "dirFichero":
				dirFichero(posicionActual.getEstoy());
				break;
			case "help":
				help();
				break;
				
			case "mkdir":
				mkdir(posicionActual.getEstoy().toString());
				break;
			case "write":
				
				write(posicionActual.getEstoy().toString());
				break;
				
			case "close":
				close();
				break;
				
			default:
				System.out.println("\"" + eleccionUsuario +"\""  + " no se reconoce como un comando interno o externo" + "\n");
			}
			
				
			}
		}
	

	/**
	 * Funcion que nos permitira mediante el uso de una ruta absoluta, acceder a un
	 * fichero y mostrar el contenido de él.
	 * @author Sergio García
	 * @param ruta Variable que contendra la ruta introducida
	 */
	public static void cat(String ruta) {
		// El usuario ha introducido en el main la ruta absoluta
		System.out.println("Inserta la ruta con el fichero:");
		String rutaIntroducida = sc.nextLine();

		Path comprobacion = Paths.get(rutaIntroducida);
		if (comprobacion.isAbsolute()) {
			File fichero = new File(rutaIntroducida);

			try {
				// Se comprueba que el fichero existe y se ejecuta si se cumple la condicion, de
				// no ser así...
				if (fichero.exists()) {

					String cadena = "";
					BufferedReader br = new BufferedReader(new FileReader(fichero));
					while ((cadena = br.readLine()) != null) {

						System.out.println(cadena);
					}
					// Cerramos el buffer para evitar problemas con flujos no cerrados
					br.close();

					// Mensaje de error
				} else {
					System.out.println("La ruta introducida no existe");

				}
				// Descripcion del error
			} catch (Exception e) {

				e.printStackTrace();

			}

		} else {
			Path rutaFinal = Paths.get(ruta).resolve(rutaIntroducida);
			File fichero = new File(rutaFinal.toString());

			try {
				// Se comprueba que el fichero existe y se ejecuta si se cumple la condicion, de
				// no ser asi...
				if (fichero.exists()) {

					String cadena = "";
					BufferedReader br = new BufferedReader(new FileReader(fichero));
					while ((cadena = br.readLine()) != null) {

						System.out.println(cadena);
					}
					// Cerramos el buffer para evitar problemas con flujos no cerrados
					br.close();

					// Mensaje de error
				} else {
					System.out.println("La ruta introducida no existe");

				}
				// Descripción del error
			} catch (Exception e) {

				e.printStackTrace();

			}
		}

	}

	/**
	 * Funcion que nos permitira ver determinadas lineas de un fichero. Uso de ruta
	 * absoluta.
	 * @author Sergio Garcia
	 * @param eleccionUsuario Valor que determinara el numero de lineas que se mostraran
	 * @param ruta Valor que recoge la ruta que se ha de mostrar
	 */
	public static void top(int eleccionUsuario, String ruta) {
		// El usuario ha introducido ya en el main cuántas lineas a mostrar y que ruta
		System.out.println("Lineas a mostrar");
		eleccionUsuario = sc.nextInt();
		sc.nextLine();
		System.out.println("Inserta la ruta con el fichero:");
		String rutaIntroducida = sc.nextLine();
		Path comprobacion = Paths.get(rutaIntroducida);
		if (comprobacion.isAbsolute()) {
			File fichero = new File(rutaIntroducida);

			try {
				if (fichero.exists()) {

					String cadena = "";
					BufferedReader br = new BufferedReader(new FileReader(fichero));
					// Bucle que recorrerá el fichero leyéndolo hasta que se cumpla la condición.
					for (int i = 0; i < eleccionUsuario; i++) {
						cadena = br.readLine();
						// Condición que nos permitirá imprimir vacio en caso de que el usuario
						// introduzca un número
						// mayor al número de líneas
						if (cadena == null) {
							break;
						} else {
							System.out.println(cadena);
						}

					}
					// Cerramos el buffer para evitar problemas de flujos
					br.close();
				} else {
					// Lanzamos una exception si el usuario introduce una ruta que no existe
					throw new Exception("La ruta introducida no existe");
				}

			} catch (Exception e) {
				System.out.println("No existe");

			}
		} else {
			Path rutaFinal = Paths.get(ruta).resolve(rutaIntroducida);
			File fichero = new File(rutaFinal.toString());
			try {
				if (fichero.exists()) {

					String cadena = "";
					BufferedReader br = new BufferedReader(new FileReader(fichero));
					// Bucle que recorrera el fichero leyendolo hasta que se cumpla la condicion.
					for (int i = 0; i < eleccionUsuario; i++) {
						cadena = br.readLine();
						// Condición que nos permitira imprimir vacio en caso de que el usuario
						// introduzca un numero
						// mayor al número de lineas
						if (cadena == null) {
							break;
						} else {
							System.out.println(cadena);
						}

					}
					// Cerramos el buffer para evitar problemas de flujos
					br.close();
				} else {
					// Lanzamos una exception si el usuario introduce una ruta que no existe
					throw new Exception("La ruta introducida no existe");
				}

			} catch (Exception e) {
				System.out.println("No existe");
				// e.printStackTrace();

			}
		}
	}
	/**
     * Metodo que primero comprueba si el metodo existe, si es asi primero comprueba si es un directorio, 
     * en tal caso llama al metodo borrarDirectorio y lo borra, si no borra el archivo y impreme por pantalla
     * un mensaje confirmando la accion
     * @author Alvaro Aguilar
     * @param fichero es una variable tipo fichero que se rellena con los valores del fichero que el usuario ha dicho
     * @param existeFichero es una booleana que se utiliza para comprobar si el fichero existe o no
     */
	public static void delete(Path ruta) {
		File fichero = new File(ruta.toString());
		boolean existeFichero = fichero.exists();
		if (existeFichero) {
			try {

				if (fichero.isDirectory()) {
					// Si es un directorio primero hay que comprobar que esta vacio para poder
					// borrralo
					borrarDirectorio(fichero);
				}

				// Borramos fichero
				fichero.delete();

				System.out.println("Los ficheros han sido eliminados satisfactoriamente");

			} catch (Exception e) {
				System.out.println("Error al eliminar los ficheros --> " + e);
			}
		} else {
			System.out.println("Error: No existe");
		}
	}
	/**
     * Metodo que nos ayuda a borrar un directorio recorriendo un arrays y comprobnado si es un directorio o no
     * si no lo es lo borra directamente y si lo es vuelve a llamar el metodo borrarDirectorio y repite el proceso
     * 
     * @author Alvaro Aguilar
     * @param ficheros es un arrays de tipo File que rellena el arrays con los archivos del fichero en cuestion
     */
	public static void borrarDirectorio(File directorio) {
		File[] ficheros = directorio.listFiles();

		for (int x = 0; x < ficheros.length; x++) {

			if (ficheros[x].isDirectory()) {
				borrarDirectorio(ficheros[x]);
			}
			ficheros[x].delete();
		}
	}
	/**
     * Retrocede a la ruta padre
     *@author Qiang Zhan
     */
	public static void directorioPadre() {
		Path ruta = posicionActual.getEstoy();
		ruta = ruta.getParent();
		posicionActual.setEstoy(ruta);
		
	}
	/**
     * @param directorio Recoge del main un Scanner y comprueba si existe
     * si se da el caso crea una nueva ruta
     *
     *@author Qiang Zhan
     */
	public static void entradaArchivo(String directorio) {
		Path ruta = posicionActual.getEstoy();
		Path nuevaRuta = ruta.resolve(directorio);
		if (nuevaRuta.toFile().exists() && nuevaRuta.toFile().isDirectory()) {
			posicionActual.setEstoy(nuevaRuta);
		} else {
			System.out.println("No es un directorio valido");
		}
	}
	/**
     * @param url Recoge del main un Scanner y comprueba si existencia para
     * posteriormente asignarla a la clase Posicion
     *
     *@author Qiang Zhan
     */
	public static void rutaAbsoluta(String url) {
		Path ruta = Paths.get(url);
		File archivo = new File(url);
		if (archivo.exists()) {
			posicionActual.setEstoy(ruta);
		} else {
			System.out.println("Error: No existe");
		}
	}
	/**
	 * La funcion nos permitirá ver la información de la ruta actual
	 * @param ruta Parámetro que recogerá en el main la ruta actual
	 * @author Oscar Yataco
	 */
	public static void info(Path ruta) {
		
		System.out.println("Introduce la ruta de la carpeta: ");
		/**
		 * Gurdamos la ruta en la variable rutaCarpeta
		 */
		String rutaCarpeta = sc.nextLine();
		/**
		* La clase File se usa para obtener información sobre archivos y directorios, permitiendo crear y eliminar archivos.
		*/
		File archivo = new File(rutaCarpeta);
		/**
		* La clase Path realiza operaciones comunes, determina si una extension de nombre de archivo forma parte de una ruta de acceso.
		*/
		Path rutaAlternativa = Paths.get(rutaCarpeta);
		if (archivo.exists()) {
			/**
			* Si el archivo existe mostrará los datos.
			* Si no existe mostrará que la ruta no existe.
			*/
			try {
				System.out.println("Nombre de la carpeta: " + archivo.getName());
				System.out.println("Ruta:                 " + archivo.getPath());
				System.out.println("Ruta absoluta:        " + archivo.getAbsolutePath());
				System.out.println("Se puede escribir:    " + archivo.canRead());
				System.out.println("Se puede leer:        " + archivo.canWrite());
				System.out.println("Tamaño:               " + archivo.length());
				System.out.println("Espacio libre:        " + archivo.getFreeSpace());
				System.out.println("Espacio usado:        " + archivo.getUsableSpace());
				System.out.println("Espacio total:        " + archivo.getTotalSpace());
				System.out.println("Root:                 " + rutaAlternativa.getRoot());
				System.out.println("Parent:               " + rutaAlternativa.getParent());
				System.out.println("Numero de elementos:  " + rutaAlternativa.getNameCount());
			} catch (Exception e) {
				e.printStackTrace();
			}
			/**
			* Si el archivo no existe confirmará que no existe.
			*/
		} else {
			System.out.println("La ruta no existe");
		}
	}
/**
 * Función que nos permitirá crear un archivo y escribir información en él
 * @author Oscar Yataco
 * @param ruta
 */
	public static void mkfile(Path ruta) {
		String texto;
		System.out.println("Introduce el nombre del archivo:");
		/**
		 * Gurdamos el nombre del archivo en un String(nombreArchivo)
		 */
		String nombreArchivo = sc.nextLine();
		/**
		* La clase File se usa para obtener información sobre archivos y directorios, permitiendo crear y eliminar archivos.
		*/
		File archivo = new File(ruta + "\\" + nombreArchivo);

		if (!archivo.exists()) {
			/**
			* Si el archivo no existe creará un archivo a través de un try catch y controlaremos los errores y crearemos el archivo.
			* Pediremos que escriba lo que desee con un BufferedWriter.
			* Este archivo se actualizará cada vez que escriba algo.
			* Al finazlizar cerraremos el BufferedWriter con un close.
			*/
			try {
				System.out.println("Archivo creado. ");
				archivo.createNewFile();
				BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo));
				System.out.println("Introduzca el texto que desea introducir en el archivo:");
				texto = sc.nextLine();
				escribir.write(texto);
				System.out.println("Actualizado.");
				escribir.close();
			} catch (IOException e) {

				e.printStackTrace(); // Nos manda el error que pueda ocurrir.
				System.err.println("No se ha podido crear el archivo");
			}

		} else {
			/**
			* Si el archivo existe nos lo confirmará
			*/
			System.out.println("El archivo ya existe.");
			/**
			* Pediremos que escriba lo que desee con un BufferedWriter.
			* Este archivo se actualizará cada vez que escriba algo.
			* Al finazlizar cerraremos el BufferedWriter con un close.
			*/
			try {
				BufferedWriter escribir = new BufferedWriter(new FileWriter(archivo));
				System.out.println("Introduzca el texto que desea introducir en el archivo:");
				texto = sc.nextLine();
				escribir.write(texto);
				System.out.println("Actualizado.");
				escribir.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Funcion que nos permitirá "borrar" todo lo que tengamos en la consola, mediante la impresion de
	 * saltos de linea
	 */
	public static void clear() {
		for (int i = 0; i < 30; i++) {
			System.out.println("");
		}

	}

	

	/**
	 * Crea un directorio con el nombre deseado en la ruta actual
	 * 
	 * @param url actual String
	 * @author Alejandro Martin Jimenez
	 */
	    public static void mkdir(String url) {
	        System.out.println("Dime como quieres que se llame el nuevo directorio");
	        String nombreDir = sc.nextLine();
	        File directorio = new File(url, nombreDir);
	        if (!directorio.exists()) {//Prueba de errores para comprobar que el directorio que se va a crear no existe
	            boolean creacion = directorio.mkdir(); // Crea el directorio
	            if (creacion) {
	                System.out.println("Se ha creado un directorio con el nombre " + nombreDir + " en la ruta " + directorio.getAbsolutePath());

	            }

	        }

	    }
	    /**
	     * Te imprime por pantalla la función de todos los comandos del terminal.
	     * 
	     * @author Alejandro Martin Jimenez
	     */
	        public static void help() {
	        	System.out.println("\n");
	            System.out.println("● cd-> Muestra el directorio actual. \n"
	                    + "\t ▪[..] -> Accede al directorio padre. \n"
	                    + "\t ▪[<nombreDirectorio>]-> Accede a un directorio dentro del directorio actual. \n"
	                    + "\t ▪[<rutaAbsoluta]-> Accede a la ruta absoluta del sistema. \n"
	                    + "● mkdir -> Crea un directorio en la ruta actual. \n"
	                    + "● info -> Muestra la informacion del elemento. Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace. \n"
	                    + "● cat  -> Muestra el contenido de un fichero. \n"
	                    + "● top  -> Muestra las líneas especificadas de un fichero. \n"
	                    + "● mkfile -> Crea un fichero con ese nombre y el contenido de texto. \n"
	                    + "● write  -> Añade 'texto' al final del fichero especificado. \n"
	                    + "● dir -> Lista los archivos o directorios de la ruta actual. \n"
	                    + "\t dirDirectorio-> Lista los archivos o directorios dentro de ese directorio. \n"
	                    + "\t dirFichero-> Lista los archivos o directorios dentro de esa ruta. \n"
	                    + "● delete  -> Borra el fichero, si es un directorio borra todo su contenido y a si mismo.\n"
	                    + "● close -> Cierra el programa. \n"
	                    + "● Clear -> Vacia la vista." + "\n");
	        }
	    

	    
	    /**
	     * Función que hará 30 saltos de línea y luego finalizará la JVM
	     */
	   public static void close() {
		   for (int i = 0; i < 30; i++) {
			   System.out.println("");
		}
		   System.exit(0);
	   }
	   /**
	     * Metodo que primero pide el nombre del archivo que quieres modificar, 
	     * luego comprueba si existe y despues si es un file, y si es asi pide el texto que quieres añadir
	     * y suma ese texto a lo que ya estaba creado en ese archivo anteriormente.
	     * 
	     * @param nombreFichero es una variable tipo String que se rellena con el nombre del archivo introducido por el usuario
	     * @param f es una variable de la clase File que se rellena de el valor de la ruta y del nombreFichero 
	     * @param texto es una variable tipo String que se rellena con lo que el usuario quiere añadir al archivo
	     * @param bw es una variable de la clase BufferedWriter y se rellena con la variable f y un true que permiete añadir texto en vez de remplazarlo
	     * 
	     * @throws IOException ex si hay un error a la hora de sobrescribir el archivo
	     */
	   public static void write(String ruta) {

	        //Compruebo si esa ruta existe y si es un fichero. 
	        System.out.println("¿Como se llama el archivo?");
	        String nombreFichero = sc.nextLine();
	        File f = new File(ruta + "\\" + nombreFichero);
	        if (f.exists()) {
	            System.out.println(f);
	            if (f.isFile()) {
	                System.out.println("Escribe el texto que quieres añadir.");
	                String texto = sc.nextLine();
	                try {
	                    BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
	                    bw.write(texto);
	                    bw.close();

	                } catch (IOException ex) {
	                    System.out.println("Error");
	                    System.out.println(ex);
	                }
	            } else {
	                System.out.println("No es un fichero.");
	            }
	        } else {
	            System.out.println("No existe la ruta.");
	        }
	    }
	   /**
        *  Funcion que nos permitira listar los archivos de la ruta absoluta 
        *          introducida por el usuario
        * 
        * 
        * @author Ruben Soriano
        */
	   public static void dirRuta() {
	        System.out.println("Introduzca la ruta: ");
	        final String Nombre_Directorio = sc.nextLine();

	        try ( DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(Nombre_Directorio))) {
	            for (Path ruta : ds) {
	                System.out.println(ruta.getFileName());
	            }
	        } catch (IOException ex) {
	            System.out.println("Error");
	            System.out.println(ex);
	        }
	    }
	   /**
        *  Funcion que nos listara los archivos de la posición actual del usuario
        * @param nombreRuta variable de tipo String que nos permite obtener la ruta actual en la que te encuentras
        */
	   public static void dir(String nombreRuta) {
	        try ( DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(nombreRuta))) {
	            for (Path ruta : ds) {
	                System.out.println(ruta.getFileName());
	            }
	        } catch (IOException ex) {
	            System.out.println("Error");
	            System.out.println(ex);
	        }
	    }
	   /**
        *  Funcion que nos permitira listar el contenido del fichero elegido 
       *             por el usuario dentro de la ruta en la que se encuentra
        * @param ruta variable de tipo String que nos permite obtener el  Path de la ruta en la que se encuentra el ususario
        */
	   public static void dirFichero(Path ruta) {

	        System.out.println("Introduce el nombre del directorio: ");
	        String nombre = sc.nextLine();

	        File directorio = new File(ruta.toString() + "\\" + nombre);
	        boolean existeDirectorio = directorio.exists();

	        if (existeDirectorio) {

	            if (directorio.isDirectory()) {
	                try ( DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(directorio.toString()))) {
	                    for (Path rutaN : ds) {
	                        System.out.println(rutaN.getFileName());
	                    }
	                } catch (IOException ex) {
	                    System.out.println("Error");
	                    System.out.println(ex);
	                }
	            } else {
	                System.out.println("No es un directorio.");
	            }
	        } else {
	            System.out.println("No existe.");
	        }
	    }
	   
	   
	   
}

	

	
	



