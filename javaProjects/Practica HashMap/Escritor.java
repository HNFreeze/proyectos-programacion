package practicaProgramacion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class Escritor {

	private static String path;

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		Escritor.path = path;
	}

	/**
	 * Metodo que permite escribir en un fichero una ID de un alumno junto con su
	 * nota respectiva
	 * 
	 * @param idAlumno   Valor identificativo de un alumno, no se puede repetir
	 * @param notaAlumno Nota que tiene un alumno
	 * @throws IOException
	 */
	public static void escribe(int idAlumno, double notaAlumno) throws IOException {

		File fichero = new File(getPath());

		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(fichero, true)));
		pw.println(idAlumno + " " + notaAlumno);
		pw.close();

	}

	/**
	 * Sobreescribe el fichero por completo. En caso de que, en tiempos de ejecucion
	 * del main, se le pasen argumentos, permitira sobreescribir la nota de un
	 * alumno, si la id existe
	 * 
	 * @param mapa Valor que permite en GestionaNotas sobreescribir el HashMap
	 *             principal
	 * @throws IOException
	 */
	public static void escribeTodo(HashMap<Integer, Double> mapa) throws IOException {

		File fichero = new File(getPath());
		BufferedWriter bw = new BufferedWriter(new FileWriter(fichero));
		bw.write("");
		bw.close();
		// Con el bucle conseguimos que se rellene todo nuestro fichero y mapa de nuevo
		// con la nueva nota
		for (Integer idAlumno : mapa.keySet()) {
			//Usamos el metodo escribe para escribir todas las Ids y notas que hab�an, junto con la nueva nota
			escribe(idAlumno, mapa.get(idAlumno));
		}
	}

}
