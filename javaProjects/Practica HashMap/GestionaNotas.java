package practicaProgramacion;

import java.io.IOException;
import java.util.HashMap;

public class GestionaNotas {
	private static HashMap<Integer, Double> mapa = new HashMap<>();

	/**
	 * @author Sergio Garcia Sanchez 
	 * @author Carlos Martin Gri�an
	 * @param args Argumentos introducidos en tiempo de ejecucion
	 */
	public static void main(String[] args) {
		//Creamos dos variables finales que las usaremos para que no se pueda introducir una nota menor a 0 y mayor a 10.
		final Double NOTA_MAXIMA = 10.0;
		final Double NOTA_MINIMA = 0.0;
		//Establecemos la ruta que vamos a usar
		Lector.setPath(args[1]);
		Escritor.setPath(args[1]);
		

		// Posici�n 0 = w, escritura
		if (!(args[0].equals("w") || args[0].equals("r") || args[0].equals("m"))) {
			System.err.println("El primer argumento debe ser [w] , [r] o [m]");
		}
		if (args[0].equals("w")) {
			//Comprobaci�n nota minima y maxima a introducir
			if (Double.parseDouble(args[3]) > NOTA_MAXIMA) {
				System.err.println("La nota m�xima es " + NOTA_MAXIMA);
				System.exit(0);//Interrumpimos la ejecuci�n
			} else if (Double.parseDouble(args[3]) < NOTA_MINIMA) {
				System.err.println("La nota m�nima es " + NOTA_MINIMA);
				System.exit(0);//Interrumpimos la ejecuci�n
			}

			try {
				try {
					//Nuestro mapa(HashMap) del main, en una primera instancia es nulo, pero al igualarlo a nuestro
					//metodo controlLectura(), el cual devuelve un HashMap, ya no sera nulo y podremos trabajar con el
					mapa = Lector.controlLectura();
				} catch (IOException e1) {
					//Este catch, saltar� cuando intentemos crear el archivo, ya que no encontrar� la ruta,
					//pero igualmente lo crear�, por eso lo capturamos para poder modificar el mensaje que salta
					System.out.println("Archivo creado con �xito");
				}

				if (mapa.containsKey(Integer.parseInt(args[2]))) {
					System.err.println("La ID introducida ya existe");
				} else {

					Escritor.escribe(Integer.parseInt(args[2]), Double.parseDouble(args[3]));
					System.out.println("Se ha escrito con �xito en el fichero");
				}

			} catch (NumberFormatException e) {
				System.err.println("Debes introducir un valor correcto");

			} catch (IndexOutOfBoundsException e) {
				System.err.println("Faltan argumentos");
			} catch (IOException e) {
				System.err.println("Error de entrada/salida");
			}

			// Posici�n 0 = r, lectura
		} else if (args[0].equals("r")) {

			try {
				//Nuestro mapa(HashMap) del main, en una primera instancia es nulo, pero al igualarlo a nuestro
				//metodo controlLectura(), el cual devuelve un HashMap, ya no sera nulo y podremos trabajar con el
				mapa = Lector.controlLectura();

				double idNoEncontrada = Lector.lee(Integer.parseInt(args[2]));
				if (idNoEncontrada == -1) {
					System.err.println("No hay ningun alumno con esa ID");
				} else {
					System.out.println(Lector.lee(Integer.parseInt(args[2])));
				}

			} catch (IOException e) {
				System.err.println("El archivo introducido no existe");
			} catch (NumberFormatException e) {
				System.err.println("Debes introducir un valor correcto");
			} catch (IndexOutOfBoundsException e) {
				System.err.println("Faltan argumentos");
			}

			// Posici�n 0 = m, modificaci�n de una nota
		} else if (args[0].equals("m")) {

			if (Double.parseDouble(args[3])> NOTA_MAXIMA) {
				System.err.println("La nota m�xima es " + NOTA_MAXIMA);
				System.exit(0);
			} else if (Double.parseDouble(args[3]) < NOTA_MINIMA) {
				System.err.println("La nota m�nima es " + NOTA_MINIMA);
				System.exit(0);
			}

			try {
				//Nuestro mapa(HashMap) del main, en una primera instancia es nulo, pero al igualarlo a nuestro
				//metodo controlLectura(), el cual devuelve un HashMap, ya no sera nulo y podremos trabajar con el
				mapa = Lector.controlLectura();

				if (mapa.containsKey(Integer.parseInt(args[2]))) {
					mapa.replace(Integer.parseInt(args[2]), Double.parseDouble(args[3]));

					Escritor.escribeTodo(mapa);
					System.out.println("Nota modificada con �xito");

				} else {
					System.err.println("La ID introducida no existe");
				}
			} catch (IOException e) {
				System.err.println("El archivo introducido no existe");
			} catch (NumberFormatException e) {
				System.err.println("Debes introducir un valor correcto");
			}

		}

	}
}
