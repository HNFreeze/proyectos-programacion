package practicaprogramacion;

import java.util.Scanner;

/**
 * Práctica 1
 *
 * @author SergioGarcíaSánchez
 */
public class MenúBucle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Bienvenido");
        System.out.println("─▌█──║─║╔═║─║─╔═╗─\n" +
"─███─╠═╣╠═║─║─║─║─\n" +
"─▐█▐─║─║╚═╚═╚═╚═╝─\n" +
"─▐▐───────────────\n" +
"─▐▐───────────────"+"\n");
        Scanner sc = new Scanner(System.in);
        int selector;//Selector del menú
        boolean continuar = true; //FIN DEL PROGRAMA (When FALSE)
        double resultado2 = 0;//Variable resultado2 es una variable en la que se almacenará el resultado de la calculadoraX
        boolean control = false;//Esta variable hará que funcione nuestro bucle do-while
        //Primero empezamos haciendo el menú:
        //Este menú tenemos que meterlo dentro de un bucle ya que cuando terminemos un programa, volveremos al menú para elegir que programa usar.

        while (continuar == true) {

            System.out.println("1. Calculadora" + "\n2. Ver último resultado de la calculadora" + "\n3. Número primo" + "\n4. Cuadrícula"
                    + "\n5. Pirámide de números" + "\n6. Información de la aplicación" + "\n0. Salir");

            do {    //Este bucle do-while es creado con el objetivo de que si introducimos un valor del menú(valor entero)que no esté, te de un mensaje de que no puedes introducir ese número.   

                selector = sc.nextInt();

                if (selector == 1) {//CALCULADORA
                    
                    //Declaramos las variables de la calculadora, piden tipo char por lo que inicializamos un tipo char, junto con 2 variables, que seran los valores a operar.
                    double num1;
                    double num2;//resultado2 es el menú que devuelve
                    System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
                    System.out.println("Escribe el primer operando: ");
                    num1 = sc.nextDouble();
                    System.out.println("Escribe el segundo operando: ");
                    num2 = sc.nextDouble();
                    System.out.println("Escribe la operación [+,-,*,/]");

                    double result = calculadoraX(num1, num2);
                    resultado2 = result;//Aquí igualo la variable que hemos creado al resultado de nuestra calculadora, para almacenar el resultado.
                    System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>");

                }

                if (selector == 2) {//RESULTADO OPERACIÓN

                    System.out.println("El último resultado de la calculadora fue " + resultado2);
                    System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" + "\n");
                }
                if (selector == 3) { //NÚMEROS PRIMOS
                    int selectorPrimo = 0;
                    System.out.println("1. Ver si el número introducido es primo" + "\n2. Mostrar los números primos hasta un valor");
                    selectorPrimo = sc.nextInt();
                    if(selectorPrimo == 1){
                        System.out.println("Introduce un número");
                        int numero1 = sc.nextInt();
                    boolean resultadoPrimo = numeroPrimo(numero1);
                    if(resultadoPrimo == true){
                        System.out.println(" El número " + numero1 + " es primo ");
                    }else{
                        System.out.println("El número " + numero1 + " no es primo");
                    }
                    } else if(selectorPrimo == 2){
                        System.out.println("Introduce un número");
                        int numero2 = sc.nextInt();
                        
                        rangoNumeroPrimo(numero2);
                        
                    }
                    
                }
                if (selector == 4) {//CUADRÍCULA
                    int filas;
                    int columnas;
                    char relleno = 0;
                    System.out.println("Introduce el número de filas: ");
                    filas = sc.nextInt();
                    System.out.println("Introduce el número de columnas: ");
                    columnas = sc.nextInt();
                    System.out.println("Introduce el caracter de relleno: ");
                    cuadriculaX(filas, columnas, relleno); //Llamamos a nuestro método llamado cuadriculaX.

                }
                if (selector == 5) {//PIRÁMIDE
                    int piramide = 0;
                    System.out.println("Introduce el tamaño de la pirámide de números: ");
                    piramide = sc.nextInt();
                    if (piramide >= 0) {
                       piramidePositiva(piramide);
                    } else {
                      piramideNegativa(piramide);
                    }

                }
                if (selector == 6) {//INFORMACIÓN
                    System.out.println("© Sergio García Sánchez ® \n" +
                                            "█║▌│█│║▌║││█║▌║▌║ ");
                    System.out.println("Curso: 1ºDAM DUAL");
                    
                    System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>" + "\n");
                }
                if (selector == 0) {//FINALIZAR
                    System.out.println("FIN DEL PROGRAMA" +"\n");
                    continuar = false; //Igualamos nuestra variable continuar(la cual es una booleana) a false, ya que nuestro bucle funciona mientras sea true.
                }
                if (selector < 0 || selector > 6) {//CONTROL DE ERRORES
                    control = true;
                    System.out.println("NO EXISTE ESA OPCIÓN, ELIGE UNA OPCIÓN DEL MENÚ" + "\n");
                }
            } while (control = false);
        }
    }

    public static double calculadoraX(double num1, double num2) { //Mediante el uso de un switch el cual es de tipo char,establecemos los casos a realizar.
        Scanner sc = new Scanner(System.in);
        char calculadora = sc.next().charAt(0);
        double resultado = 0;
        switch (calculadora) {

            case '+':
                resultado = num1 + num2;
                System.out.println("El resultado de " + num1 + " + " + num2 + " = " + resultado);
                break;

            case '-':
                resultado = num1 - num2;
                System.out.println("El resultado de " + num1 + " - " + num2 + " = " + resultado);
                break;

            case '*':
                resultado = num1 * num2;
                System.out.println("El resultado de " + num1 + " * " + num2 + " = " + resultado);
                break;

            case '/':
                resultado = num1 / num2;
                System.out.println("El resultado de " + num1 + " / " + num2 + " = " + resultado);
                break;

        }
        return resultado;

    }

    public static void cuadriculaX(int fila, int columna, char relleno) { //Creamos un método que no devuelve nada, en el cual inicializamos un bucle anidado. El primero declara nuestras filas, y el segundo las columnas.
        Scanner sc = new Scanner(System.in);                              //Es importante imprimir el relleno mediante un print, y no un println, ya que nos imprimiría todo hacia abajo, cosa que me ha dado problema
        relleno = sc.next().charAt(0);                                    //hasta que me he dado cuenta. :(
        for (int i = 0; i < fila; i++) { //Filas
            for (int j = 0; j < columna; j++) { //Columnas
                System.out.print(relleno + " "); //Imprime el relleno que habrá en la matriz

            }
            System.out.println(" ");

        }

    }
//Decido hacer uso de dos funciones para separar la piramide positiva con la invertida, por facilidad y claridad.
    public static int piramidePositiva(int piramide) { 
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < piramide + 1; i++) { //En el bucle, en ("i<piramide+1"), el +1 hace que podamos imprimir el número introducido, de no ser así, imprimiría un valor menos, empezando desde el 0.
            for (int j = 0; j < i; j++) {
                System.out.print(i + " "); //El imprimir la i nos permite tener nuestra pirámide.

            }
            System.out.println(" ");

        }
        return piramide;

    }

    public static int piramideNegativa(int piramide2) {
        Scanner sc = new Scanner(System.in);
        piramide2 = piramide2 * (-1); //Si no lo multiplicamos por -1, nos imprimirá números con el signo menos.
        for (int i = piramide2; i >= 0; i--) {
            for (int j = i; j >= 1; j--) {
                System.out.print(i + " ");

            }
            System.out.println("");

        }
        return piramide2;
    }
    public static boolean numeroPrimo(int num){ //NºPrimo es un número que solo es divisible por si mismo y la unidad, por lo que:
        int numero = 2; //Establecemos una variable que comienza en el 2 para luego usarla como divisor
        boolean primo = true;
        while((primo)&&(numero!=num)){
            if(num%numero == 0){ //Si el número que introducimos, lo dividimos entre el número 2 y tiene como resto 0, nos devolverá que no es primo.
                primo = false;   //Por su contraparte, si la anterior frase no se cumple, el número 2 se sumará y volverá al bucle y hará todo el rato la división
                break;           //hasta que encuentre un/unos números que no den resto 0.
            }
            numero++;
        }
        return primo;
            
    }
    public static void rangoNumeroPrimo(int numero){ //Aquí hacemos un llamamiento a la primera parte del ejercicio de primos, ya que así podemos hacer los rangos que tiene un número y clasificar por primos.
        for (int i = 2; i < numero; i++) {           //De tal forma que si introducimos el nº9, de la primera forma solo nos diría el 1 y el 3, pero de esta forma nos dirá el 2,3,5,7.
            if(numeroPrimo(i)){
                System.out.println(i);
            }
            
            
        }
    }

}
