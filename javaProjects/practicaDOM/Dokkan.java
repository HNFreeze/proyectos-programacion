package practicaDOM;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class Dokkan {

	static String ficheroXML = "dokkanXML.xml";
	static Document documento;
	static Path ruta = Paths.get(System.getProperty("user.dir"));
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		boolean finPrograma = true;
		System.out.println("Programa que permite leer y editar un xml, y borrar elementos de �l."
				+ "\nRealizado por Sergio Garc�a S�nchez" + "\n");
		do {
			System.out.println(
					"1.Leer xml" + "\n2.A�adir elementos al xml" + "\n3.Borrar elementos del xml" + "\n4.Salir");
			int selectorMenu = sc.nextInt();

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			DocumentBuilder builder;
			try {
				builder = dbf.newDocumentBuilder();
				documento = builder.parse(new File(ficheroXML));
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			switch (selectorMenu) {
			case 1:
				leerXML();
				break;
			case 2:
				a�adirElementos();

				break;
			case 3:
				borrarElemento();
				break;

			case 4:
				finPrograma = false;
				break;

			default:
				System.out.println("Introduce un valor entre el rango (1 | 2 | 3 | 4)" + "\n");
				break;

			}

		} while (finPrograma);
		{
			System.out.println("Fin del programa");
		}

	}

	public static void leerXML() {

		try {

			System.out.println("Elemento ra�z: " + documento.getDocumentElement().getNodeName());

			NodeList personajes = documento.getElementsByTagName("personaje");

			for (int i = 0; i < personajes.getLength(); i++) {
				System.out.println("->Personaje");
				Node personaje = personajes.item(i);

				if (personaje.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) personaje;

					// ---- Nombre personaje
					NodeList nodoNombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes();
					Node valorNodo = (Node) nodoNombre.item(0);
					System.out.println("\tNombre: " + valorNodo.getNodeValue());

					// ---- Habilidad personaje
					NodeList nodoHabilidad = elemento.getElementsByTagName("habilidad").item(0).getChildNodes();
					Node valorNodo2 = (Node) nodoHabilidad.item(0);
					System.out.println("\tHabilidad: " + valorNodo2.getNodeValue());

					// ---- Edad personaje
					NodeList nodoEdad = elemento.getElementsByTagName("edad").item(0).getChildNodes();
					Node valorNodo3 = (Node) nodoEdad.item(0);
					System.out.println("\tEdad: " + valorNodo3.getNodeValue());

					// ---- Raza del personaje
					NodeList nodoRaza = elemento.getElementsByTagName("raza").item(0).getChildNodes();
					Node valorNodo4 = (Node) nodoRaza.item(0);
					System.out.println("\tRaza: " + valorNodo4.getNodeValue());
				}
			}

			System.out.println(" ");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void a�adirElementos() {
		sc.nextLine();
		System.out.println("Inserte el nombre del personaje: ");
		String nombrePersonaje = sc.nextLine();

		System.out.println("Inserte la habilidad del personaje: ");
		String habilidadPersonaje = sc.nextLine();

		System.out.println("Inserte la edad del personaje ");
		String edadPersonaje = sc.nextLine();

		System.out.println("Inserte la raza del personaje: ");
		String razaPersonaje = sc.nextLine();

		Element raizPersonaje = documento.createElement("personaje");
		documento.getDocumentElement().appendChild(raizPersonaje);

		// Creaci�n de elemento nombre
		Element nombreElemento = documento.createElement("nombre");
		Text nombreTexto = documento.createTextNode(nombrePersonaje);
		raizPersonaje.appendChild(nombreElemento);
		nombreElemento.appendChild(nombreTexto);

		// Creaci�n de elemento habilidad
		Element habilidadElemento = documento.createElement("habilidad");
		Text habilidadTexto = documento.createTextNode(habilidadPersonaje);
		raizPersonaje.appendChild(habilidadElemento);
		habilidadElemento.appendChild(habilidadTexto);

		// Creaci�n de elemento edad
		Element edadElemento = documento.createElement("edad");
		Text edadTexto = documento.createTextNode(edadPersonaje);
		raizPersonaje.appendChild(edadElemento);
		edadElemento.appendChild(edadTexto);

		// Creaci�n de elemento raza
		Element razaElemento = documento.createElement("raza");
		Text razaTexto = documento.createTextNode(razaPersonaje);
		raizPersonaje.appendChild(razaElemento);
		razaElemento.appendChild(razaTexto);

		// Escribimos en xml
		
		Source fuenteXML = new DOMSource(documento);

		Result resultadoEscritura = new StreamResult(ficheroXML);// Sobreescribimos

		try {
			Transformer transformacion = TransformerFactory.newInstance().newTransformer();
			transformacion.transform(fuenteXML, resultadoEscritura);
			System.out.println("XML sobreescrito con �xito, ubicaci�n del archivo: " + ruta);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

	}

	public static void borrarElemento() {
		sc.nextLine();
		System.out.println("Inserta el nombre del personaje que quieres borrar: ");
		String elementoBorrar = sc.nextLine();

		NodeList personajes2 = documento.getElementsByTagName("personaje");

		for (int i = 0; i < personajes2.getLength(); i++) {
			Node personaje2 = personajes2.item(i);
			if (personaje2.getNodeType() == Node.ELEMENT_NODE) {
				Element elemento = (Element) personaje2;
				NodeList nodo = elemento.getElementsByTagName("nombre").item(0).getChildNodes();
				Node valorNodo = (Node) nodo.item(0);

				if (valorNodo.getNodeValue().equals(elementoBorrar)) {
					personaje2.getParentNode().removeChild(personaje2);
				}

				try {
					Source source = new DOMSource(documento);
					Result resultado = new StreamResult(ficheroXML);
					Transformer transformacion = TransformerFactory.newInstance().newTransformer();
					transformacion.transform(source, resultado);

				} catch (TransformerConfigurationException e) {
					e.printStackTrace();
				} catch (TransformerFactoryConfigurationError e) {
					e.printStackTrace();
				} catch (TransformerException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(elementoBorrar + " ha sido borrado" + "\nLa ubicaci�n del archivo es: " + ruta);
	}

}
