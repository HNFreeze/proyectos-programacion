package administracionEmail;

import java.util.Scanner;

public class Email {
	private String primerNombre;
	private String apellido;
	private String contrase�a;
	private String departamento;
	private String correo;
	private int capacidadMail = 500;
	private int longitud_por_defecto_Contrase�a = 5;
	private String emailSecundario;
	private String sufijoCompa�ia = "cesur.com";
	Scanner sc = new Scanner(System.in);

	public String getPrimerNombre() {
		return primerNombre;
	}

	public String getApellido() {
		return apellido;
	}

	// Constructor para recibir el primer nombre y el apellido. //Cuando se crea un
	// nuevo email, mostramos mensaje de que el correo se ha creado

	public Email(String nombre, String apellido) {
		this.primerNombre = nombre;
		this.apellido = apellido;
		System.out.println("Correo creado: " + nombre + " " + apellido);
		// Aqu� inicializamos el departamento con nuestro m�todo para poder establecer
		// un departamento
		this.departamento = setDepartamento();
		System.out.println("Departamento: " + departamento);

		// Llamamos a metodo que devuelve una contrase�a aleatoria
		this.contrase�a = contrase�aAleatoria(longitud_por_defecto_Contrase�a);
		System.out.println("Contrase�a: " + contrase�a);

		// Tendremos que combinar todos los elementos para generar el correo, todo en
		// minusculas
		correo = nombre.toLowerCase() + "." + apellido.toLowerCase() + "@" + departamento.toLowerCase() + "."
				+ sufijoCompa�ia;
		System.out.println("Tu correo es: " + correo);
	}

	// Preguntar por el departamento

	private String setDepartamento() {
		System.out.print(
				"C�digos de departamento\n1 para Ventas\n2 para Desarrollo\n3 para Cuentas\n0 para Nada\nIntroduce c�digo de departamento:");
		int eleccionDepartamento = sc.nextInt();
		if (eleccionDepartamento == 1) {
			return "Ventas";
		} else if (eleccionDepartamento == 2) {
			return "Desarrollo";
		} else if (eleccionDepartamento == 3) {
			return "Cuentas";
		} else if (eleccionDepartamento == 0) {
			return "Nada";
		} else {
			return "";
		}
	}

	// Generar una contrase�a por defecto/aleatoria
	private String contrase�aAleatoria(int longitud_Contrase�a) {
		String valoresContrase�a = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789!@#�%&";
		// Creamos un array de tipo char que lo que har� es coger aleatoriamente un
		// valor de nuestra variable valoresContrase�a
		char[] contrase�a = new char[longitud_Contrase�a];
		for (int i = 0; i < contrase�a.length; i++) { // Recorremos nuestro array
			// Lo que hacemos es crear un numero aleatorio de la longitud de nuestra
			// variable valoresContrase�a,
			// y como tenemos 43 valores, pues generar� un n�mero aleatorio.
			int numeroAleatorio = (int) (Math.random() * valoresContrase�a.length());
			// Aqui lo que hacemos es que en nuestro array, como estamos recorriendo el
			// array, asignamos gracias a charAt, un valor
			// de los valores que puede tener nuestra contrase�a
			contrase�a[i] = valoresContrase�a.charAt(numeroAleatorio);
		}
		// Aqui como tenemos que devolver un String, pero nuestro array es de tipo char,
		// simplemente devolvemos un nuevo String,
		// que ser� esa variable
		return new String(contrase�a);
	}

	// Establecer una capacidad de email

	private int cambiarCapacidadMail(int capacidad) {
		if (capacidad <= 0) {
			return -1;
		} else if (capacidad == this.capacidadMail) {
			return 0;
		} else {
			this.capacidadMail = capacidad;
		}
		return capacidad;

	}

	// Email alternativo

	public void emailAlternativo(String emailAlt) {
		this.emailSecundario = emailAlt;
	}

	// Cambiar contrase�a
	public void cambiarContrase�a(String nuevaContrase�a) {
		this.contrase�a = nuevaContrase�a;
	}

	public int getCapacidadMail() {
		return capacidadMail;
	}

	public String getEmailSecundario() {
		return emailSecundario;
	}

	public String getContrase�a() {
		return contrase�a;
	}

	@Override
	public String toString() {
		return "Nombre : " + primerNombre + "\nApellido: " + apellido + "\nDepartamento: " + departamento
				+ "\nSufijo Compa�ia: " + sufijoCompa�ia + "\nContrase�a: " + getContrase�a() + "\nCapacidad Correo: "
				+ capacidadMail + "\nEmail Secundario: " + emailSecundario;
	}

}
