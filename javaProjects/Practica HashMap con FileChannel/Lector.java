package practicaPSPHashMaps;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Lector {

	private static String path;

	// Este Getter del path, recogerá el args[1] de los argumentos del main
	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		Lector.path = path;
	}

	/**
	 * 
	 * @return Devuelve un HashMap con la información que hay en el fichero de texto
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static HashMap<Integer, Double> leerTodo() throws IOException, NumberFormatException {
		HashMap<Integer, Double> mapa = new HashMap<>();

		String linea;
		BufferedReader lectorFichero;

		lectorFichero = new BufferedReader(new FileReader(getPath()));
		while ((linea = lectorFichero.readLine()) != null) {
			String[] partesFichero = linea.split(" ");
			// Con trim podemos devolver la cadena ignorando cualquier espacio en blanco a
			// la derecha o izquierda
			Integer clave = Integer.parseInt(partesFichero[0].trim());
			Double valor = Double.parseDouble(partesFichero[1].trim());
			mapa.put(clave, valor);

		}

		for (Integer identificadorAlumno : mapa.keySet()) {
			System.out.println(identificadorAlumno + " " + mapa.get(identificadorAlumno));
		}
		lectorFichero.close();

		return mapa;

	}

	/**
	 * Permite leer la nota de un alumno, introduciendo una id existente
	 * 
	 * @param idAlumno Valor que simboliza la identificacion de un alumno
	 * @return Devuelve la nota del alumno y si no existe, devuelve -1
	 * @throws IOException
	 */
	public static double lee(int idAlumno) throws IOException {
		HashMap<Integer, Double> mapaAuxiliar = new HashMap<>();
		mapaAuxiliar = controlLectura();
		double notaAlumno = 0;
		if (mapaAuxiliar.containsKey(idAlumno)) {
			notaAlumno = mapaAuxiliar.get(idAlumno);

		} else {
			return -1;
		}

		return notaAlumno;

	}

	/**
	 * Recorre un fichero, recogiendo las Ids y las notas, y lo guarda en un HashMap
	 * 
	 * @return Devuelve un HashMap con la informacion del fichero
	 * @throws IOException
	 */
	public static HashMap<Integer, Double> controlLectura() throws IOException {
		HashMap<Integer, Double> mapaAux2 = new HashMap<>();
		String lectorLineas;

		BufferedReader br = new BufferedReader(new FileReader(getPath()));
		while ((lectorLineas = br.readLine()) != null) {
			String[] partesFichero = lectorLineas.split(" ");
			Integer clave = Integer.parseInt(partesFichero[0].trim());
			Double valor = Double.parseDouble(partesFichero[1].trim());
			mapaAux2.put(clave, valor);
		}
		br.close();

		return mapaAux2;

	}
}
