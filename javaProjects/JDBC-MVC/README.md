# JDBC
Proyecto realizado por Sergio García Sánchez. Este proyecto está desarrollado con Java, en el IDE conocido como Eclipse.

Consiste en un programa que permite controlar 2 tablas de una base de datos, la cual está en los archivos para su uso.




## Como empezar
Para empezar, debes descargarte el script [base_de_datos_dokkan.sql] de la base de datos el cual está en la JDBC.

Una vez descargado, tendrás que abrirlo en MySQL Workbench y ejecutar el script.

## Archivo mysql-properties.xml

Para que el programa funcione correctamente, deberás de generar un archivo xml, el cual te permitirá conectarte a MySql Workbench, y vincular las tablas de la base

de datos y a la hora de ejecutar el programa[DokkanBattleGUI], deberéis de ir a las properties de ese .java, "Run/Debug Settings",

"Edit", "Arguments", y donde pone "Program Arguments", pegar la ruta absoluta donde tengáis el archivo .xml.

Ejemplo: F:\Programacion_Eclipse\JDBC\mysql-properties.xml

Este archivo xml tiene que tener una estructura en concreto, en la imagen que está subida, se muestra como debe de formarse.

## mysql-connector-java-8.0.26.jar Classpath

Para que el programa pueda funcionar, necesitamos incluir en el Classpath del programa un .jar, el cual está subido y podéis descargar.

Una vez descargado, y con el proyecto ya importado en Eclipse, deberéis de pulsar click derecho en el proyecto e ir a properties.

Después ir a Java Build Path, hacer click en Classpath y en las opciones de la derecha, pulsar en Add External JARs, y elegiremos el .jar que descargamos.

Apply and Close para continuar.






