package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Personaje;
import modelo.PersonajeDAO;

public class PersonajeControllerJDBC implements PersonajeDAO {
	Connection con;
	private static final String SELECT_ALL_PERSONAJES = "select * from tcharacter";
	private static final String SELECT_ALL_PERSONAJES__NOMBRE = "select * from tcharacter where name like ?";
	private static final String SELECT_PERSONAJES_POR_ID = "select * from tcharacter where Idcharacter = ?";
	private static final String SELECT_ALL_PERSONAJES_POR_ID_TIPO = "select * from tcharacter where Idtype=?";
	private static final String SELECT_ALL_PERSONAJES_POR_ID_EVENTO = "select * from tcharacter where Idevent = ?";
	private static final String INSERT_PERSONAJE = "insert into tcharacter values (?,?,?,?,?)";
	private static final String UPDATE_PERSONAJE = "update tcharacter set name=?,Idtype=?,Idevent=?,passiveskill=? where Idcharacter = ?";
	private static final String DELETE_PERSONAJE = "delete from tcharacter where Idcharacter=?";

	public PersonajeControllerJDBC(Connection con) {
		this.con = con;
	}

	@Override
	public List<Personaje> obtenerPersonajes() {
		ArrayList<Personaje> lista = new ArrayList<>();
		try {
			PreparedStatement pst = con.prepareStatement(SELECT_ALL_PERSONAJES);
			ResultSet rs = pst.executeQuery(); // rs ha recogido cada fila

			while (rs.next()) { // Mientras haya una fila disponible, recogemos los valores de nuestra BBDD en
								// variables
				int idPersonaje = rs.getInt("Idcharacter");
				String nombrePersonaje = rs.getString("name");
				int idTipo = rs.getInt("Idtype");
				int idEvento = rs.getInt("Idevent");
				String pasivaPersonaje = rs.getString("passiveskill");

				Personaje p1 = new Personaje(idPersonaje, nombrePersonaje, idTipo, idEvento, pasivaPersonaje);
				lista.add(p1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	@Override
	public List<Personaje> obtenerPersonajesPorNombre(String nombrePersonaje) {
		ArrayList<Personaje> lista = new ArrayList<Personaje>();
		try {
			PreparedStatement pst = con.prepareStatement(SELECT_ALL_PERSONAJES__NOMBRE);
			pst.setString(1, '%' + nombrePersonaje + '%');
			ResultSet rs = pst.executeQuery();
			while (rs.next()) { // Mientras haya una fila disponible, recogemos los valores de nuestra BBDD en
				// variables
				int idPersonaje = rs.getInt("Idcharacter");
				String nombre_Personaje = rs.getString("name");
				int idTipo = rs.getInt("Idtype");
				int idEvento = rs.getInt("Idevent");
				String pasivaPersonaje = rs.getString("passiveskill");
				Personaje p1 = new Personaje(idPersonaje, nombre_Personaje, idTipo, idEvento, pasivaPersonaje);
				lista.add(p1);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return lista;
	}

	@Override
	public List<Personaje> obtenerPersonajesPoridTipo(int idTipoPersonaje) {
		ArrayList<Personaje> lista = new ArrayList<Personaje>();
		try {
			PreparedStatement pst = con.prepareStatement(SELECT_ALL_PERSONAJES_POR_ID_TIPO);
			pst.setInt(1, idTipoPersonaje);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) { // Mientras haya una fila disponible, recogemos los valores de nuestra BBDD en
				// variables
				int idPersonaje = rs.getInt("Idcharacter");
				String nombre_Personaje = rs.getString("name");
				int idTipo = rs.getInt("Idtype");
				int idEvento = rs.getInt("Idevent");
				String pasivaPersonaje = rs.getString("passiveskill");
				Personaje p1 = new Personaje(idPersonaje, nombre_Personaje, idTipo, idEvento, pasivaPersonaje);
				lista.add(p1);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return lista;
	}

	@Override
	public List<Personaje> obtenerPersonajesPorIdEvento(int idEventoPersonaje) {
		ArrayList<Personaje> lista = new ArrayList<Personaje>();
		try {
			PreparedStatement pst = con.prepareStatement(SELECT_ALL_PERSONAJES_POR_ID_EVENTO);
			pst.setInt(1, idEventoPersonaje);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) { // Mientras haya una fila disponible, recogemos los valores de nuestra BBDD en
				// variables
				int idPersonaje = rs.getInt("Idcharacter");
				String nombre_Personaje = rs.getString("name");
				int idTipo = rs.getInt("Idtype");
				int idEvento = rs.getInt("Idevent");
				String pasivaPersonaje = rs.getString("passiveskill");
				Personaje p1 = new Personaje(idPersonaje, nombre_Personaje, idTipo, idEvento, pasivaPersonaje);
				lista.add(p1);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return lista;
	}

	@Override
	public Personaje obtenerPersonajePorId(int id) {
		ArrayList<Personaje> lista = new ArrayList<>();
		Personaje personaje = null;
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(SELECT_PERSONAJES_POR_ID);
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();

			// Solo es un Personaje->
			if (rs.next()) {
				id = rs.getInt("Idcharacter");
				String nombrePersonaje = rs.getString("name");
				int idTipo = rs.getInt("Idtype");
				int idEvento = rs.getInt("Idevent");
				String pasivaPersonaje = rs.getString("passiveskill");

				personaje = new Personaje(id, nombrePersonaje, idTipo, idEvento, pasivaPersonaje);
				lista.add(personaje);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return personaje;

	}

	@Override
	public boolean insertarPersonaje(int idPersonaje, String nombre, int idTipo, int idEvento, String pasiva) {
		boolean seInserta = true;
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(INSERT_PERSONAJE);
			pst.setInt(1, idPersonaje);
			pst.setString(2, nombre);
			pst.setInt(3, idTipo);
			pst.setInt(4, idEvento);
			pst.setString(5, pasiva);
			int numero = pst.executeUpdate();
			if (numero == 1) {
				seInserta = true;
			} else {
				seInserta = false;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return seInserta;

	}

	@Override
	public boolean borrarPersonaje(int id) {
		boolean seBorra = true;
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(DELETE_PERSONAJE);
			pst.setInt(1, id);
			int numero = pst.executeUpdate();
			if (numero == 1) {
				seBorra = true;
			} else {
				seBorra = false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return seBorra;

	}

	@Override
	public boolean actualizarPersonaje(Personaje nuevoPersonaje, int id_a_Actualizar) {
		boolean seActualiza = false;
		try {
			PreparedStatement pst = con.prepareStatement(UPDATE_PERSONAJE);
//update tcharacter set name=?,Idtype=?,Idevent=?,passiveskill=? where Idcharacter = ?
			pst.setString(1, nuevoPersonaje.getNombrePersonaje());
			pst.setInt(2, nuevoPersonaje.getIdEvento());
			pst.setInt(3, nuevoPersonaje.getIdEvento());
			pst.setString(4, nuevoPersonaje.getPasivaPersonaje());
			pst.setInt(5, nuevoPersonaje.getIdPersonaje());
			int numeroActualizacion = pst.executeUpdate();
			if (numeroActualizacion == 1) {
				seActualiza = true;
			} else {
				seActualiza = false;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return seActualiza;

	}

}
