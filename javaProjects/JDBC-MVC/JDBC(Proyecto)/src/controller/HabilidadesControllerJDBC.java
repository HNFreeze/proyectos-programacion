package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Habilidad;
import modelo.HabilidadDAO;

public class HabilidadesControllerJDBC implements HabilidadDAO {
	Connection con;
	private static final String SELECT_ALL_HABILIDADES = "select * from tability";
	private static final String SELECT_ALL_HABILIDADES_NOMBRE = "select * from tability where name like ?";
	private static final String SELECT_HABILIDADES_POR_ID = "select * from tability where Idability = ?";
	private static final String INSERT_HABILIDAD = "insert into tability values (?,?,?)";
	private static final String UPDATE_HABILIDAD = "update tability set name=?,effect=? where Idability = ?";
	private static final String DELETE_HABILIDAD = "delete from tability where Idability=?";

	public HabilidadesControllerJDBC(Connection con) {
		this.con = con;
	}

	@Override
	public List<Habilidad> obtenerHabilidades() {
		ArrayList<Habilidad> lista = new ArrayList<>();
		try {
			PreparedStatement pst = con.prepareStatement(SELECT_ALL_HABILIDADES);
			ResultSet rs = pst.executeQuery(); // rs ha recogido cada fila

			while (rs.next()) { // Mientras haya una fila disponible, recogemos los valores de nuestra BBDD en
								// variables
				int idHabilidad = rs.getInt("Idability");
				String nombreHabilidad = rs.getString("name");
				String efectoHabilidad = rs.getString("effect");

				Habilidad ability = new Habilidad(idHabilidad, nombreHabilidad, efectoHabilidad);
				lista.add(ability);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	@Override
	public List<Habilidad> obtenerHabilidadPorNombre(String nombreHabilidad) {
		ArrayList<Habilidad> lista = new ArrayList<Habilidad>();
		try {
			PreparedStatement pst = con.prepareStatement(SELECT_ALL_HABILIDADES_NOMBRE);
			pst.setString(1, '%' + nombreHabilidad + '%');
			ResultSet rs = pst.executeQuery();
			while (rs.next()) { // Mientras haya una fila disponible, recogemos los valores de nuestra BBDD en
				// variables
				int idHabilidad = rs.getInt("Idability");
				String nombre_Habilidad = rs.getString("name");
				String efectoHabilidad = rs.getString("effect");
				Habilidad ability = new Habilidad(idHabilidad, nombre_Habilidad, efectoHabilidad);
				lista.add(ability);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return lista;
	}

	@Override
	public Habilidad obtenerHabilidadesPorId(int idHabilidad) {
		ArrayList<Habilidad> lista = new ArrayList<>();
		Habilidad habilidad = null;
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(SELECT_HABILIDADES_POR_ID);
			pst.setInt(1, idHabilidad);
			ResultSet rs = pst.executeQuery();

			// Solo es una Habilidad->
			if (rs.next()) {
				idHabilidad = rs.getInt("Idability");
				String nombreHabilidad = rs.getString("name");
				String efectoHabilidad = rs.getString("effect");

				habilidad = new Habilidad(idHabilidad, nombreHabilidad, efectoHabilidad);
				lista.add(habilidad);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return habilidad;
	}

	@Override
	public boolean insertarHabilidad(int idHabilidad, String nombreHabilidad, String efectoHabilidad) {
		boolean seInserta = true;
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(INSERT_HABILIDAD);
			pst.setInt(1, idHabilidad);
			pst.setString(2, nombreHabilidad);
			pst.setString(3, efectoHabilidad);
			int numero = pst.executeUpdate();
			if (numero == 1) {
				seInserta = true;
			} else {
				seInserta = false;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return seInserta;
	}

	@Override
	public boolean borrarHabilidad(int idHabilidad) {
		boolean seBorra = true;
		PreparedStatement pst;
		try {
			pst = con.prepareStatement(DELETE_HABILIDAD);
			pst.setInt(1, idHabilidad);
			int numero = pst.executeUpdate();
			if (numero == 1) {
				seBorra = true;
			} else {
				seBorra = false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return seBorra;
	}

	@Override
	public boolean actualizarHabilidad(Habilidad nuevaHabilidad, int id_a_Actualizar) {
		boolean seActualiza = false;
		try {
			PreparedStatement pst = con.prepareStatement(UPDATE_HABILIDAD);
			// update tability set name=?,effect=? where Idability = ?
			pst.setString(1, nuevaHabilidad.getNombreHabilidad());
			pst.setString(2, nuevaHabilidad.getEfectoHabilidad());
			pst.setInt(3, nuevaHabilidad.getIdHabilidad());
			int numeroActualizacion = pst.executeUpdate();
			if (numeroActualizacion == 1) {
				seActualiza = true;
			} else {
				seActualiza = false;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return seActualiza;
	}

}
