package vista;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Scanner;

import controller.HabilidadesControllerJDBC;
import controller.PersonajeControllerJDBC;
import modelo.Conexion;
import modelo.Habilidad;
import modelo.Personaje;

public class DokkanBattleGUI {
	private static PersonajeControllerJDBC controllerPersonajes = null;
	private static HabilidadesControllerJDBC controllerHabilidad = null;
	private static Connection con;
	private static Personaje personajes = null;
	private static Habilidad habilidades = null;
	private static ArrayList<Personaje> listaPersonajes = new ArrayList<Personaje>();

	public static void main(String[] args) {
		// Conexi�n con la base de datos
		try {
			Conexion conexion = null;
			if (args[0] == null) {
				System.err.println("Archivo mysql-properties no especificado en la linea de comandos");
				return;
			} else {
				System.out.println("Leyendo archivo properties... " + args[0]);
				Thread.sleep(1000);
				conexion = new Conexion(args[0]);
				con = conexion.getConnection();
			}
		} catch (InvalidPropertiesFormatException e) {
			System.err.println("Error al leer el archivo properties " + args[0]);
			e.printStackTrace(System.err);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		} catch (SQLException e) {
			e.printStackTrace(System.err);
		} catch (InterruptedException e) {
			System.err.println("Hilo finalizado antes de tiempo " + e.getMessage());
		}

		controllerPersonajes = new PersonajeControllerJDBC(con);
		controllerHabilidad = new HabilidadesControllerJDBC(con);

		// Variables que se van a utilizar
		Scanner escanerLetras = new Scanner(System.in);
		Scanner escanerNumeros = new Scanner(System.in);
		int eleccionTabla;
		String nombreHabilidad;
		String efectoHabilidad;
		int selectorOpcionesPersonajes;
		int selectorOpcionesHabilidades;
		int borrarPersonajePorId;
		boolean finMenuGlobal = true;
		boolean finMenuVistaPersonajes = true;
		boolean finMenuVistaHabilidades = true;
		boolean finMenuTablaPersonajes;
		boolean finMenuTablaHabilidades;
		int idPersonaje;
		int idHabilidad;

		System.out.println("Realizado por Sergio Garcia Sanchez\nUtilizacion de base de datos DokkanBattle");
		// Primer while que controlara el menu inicial de seleccion entre Tabla de
		// Personajes y Tabla de Habilidades
		while (finMenuGlobal) {
			finMenuTablaPersonajes = true;
			finMenuTablaHabilidades = true;
			System.out.println("\n1.Tabla Personaje\n2.Tabla Habilidad\n0.Salir");
			eleccionTabla = escanerNumeros.nextInt();

			switch (eleccionTabla) {

			case 1:
				// Segundo while que controlara el menu de la Tabla de Personajes
				while (finMenuTablaPersonajes) {
					try {

						finMenuVistaPersonajes = true;
						System.out.println(
								"\nOpciones:\n1.Ver personajes\n2.Actualizar personajes\n3.Insertar personajes\n4.Borrar personajes\n0.Volver");
						selectorOpcionesPersonajes = escanerNumeros.nextInt();
						switch (selectorOpcionesPersonajes) {

						// Ver personajes
						case 1:
							System.out.println("Abriendo menu...");
							Thread.sleep(1000);
							verPersonajes(escanerLetras, escanerNumeros, finMenuVistaPersonajes);
							break;

						// Actualizar personajes
						case 2:
							System.out.print("Id personaje: ");
							idPersonaje = escanerNumeros.nextInt();
							actualizarPersonaje(escanerLetras, escanerNumeros, idPersonaje);
							break;

						// Insertar Personajes
						case 3:
							System.out.print("Id personaje: ");
							idPersonaje = escanerNumeros.nextInt();
							insertarPersonaje(escanerLetras, escanerNumeros, idPersonaje);

							break;

						// Borrar Personajes
						case 4:
							System.out.println("Introduce la id del personaje a borrar: ");
							borrarPersonajePorId = escanerNumeros.nextInt();
							borrarPersonaje(borrarPersonajePorId);

							break;

						// Volver a menu seleccion Tablas
						case 0:
							System.out.println("Volviendo...");
							Thread.sleep(1000);
							finMenuTablaPersonajes = false;
							break;

						// selectorOpcionesPersonajes < 1 || selectorOpcionesPersonajes > 4
						default:
							System.err.println("Opci�n no v�lida");
							break;
						}
					} catch (InterruptedException e) {
						System.err.println("Hilo finalizado antes de tiempo " + e.getMessage());
					}
				}

				break;

			case 2:
				// Tercer while que controlara la Tabla de Habilidades

				while (finMenuTablaHabilidades) {
					try {

						finMenuTablaHabilidades = true;
						System.out.println(
								"\nOpciones:\n1.Ver habilidades\n2.Actualizar habilidades\n3.Insertar Habilidades\n4.Borrar habilidades\n0.Volver");
						selectorOpcionesHabilidades = escanerNumeros.nextInt();
						switch (selectorOpcionesHabilidades) {

						// Ver Habilidades
						case 1:
							System.out.println("Abriendo menu...");
							Thread.sleep(1000);
							verHabilidades(escanerLetras, escanerNumeros, finMenuVistaHabilidades);
							break;

						// Actualizar Habilidades
						case 2:
							System.out.println("Introduce la id de la habilidad a actualizar: ");
							idHabilidad = escanerNumeros.nextInt();
							actualizarHabilidad(escanerLetras, idHabilidad);
							break;

						// Insertar Habilidades
						case 3:
							System.out.println("Introduce la id de la habilidad: ");
							idHabilidad = escanerNumeros.nextInt();
							habilidades = controllerHabilidad.obtenerHabilidadesPorId(idHabilidad);
							if (habilidades != null) {
								System.err.println("Ya hay una habilidad asignada a esa ID");

							} else {
								System.out.println("Nombre habilidad: ");
								nombreHabilidad = escanerLetras.nextLine();
								System.out.println("Efecto habilidad: ");
								efectoHabilidad = escanerLetras.nextLine();
								insertarHabilidad(idHabilidad, nombreHabilidad, efectoHabilidad);
							}
							break;

						// Borrar Habilidades
						case 4:
							System.out.println("Introduce la id de la habilidad a borrar: ");
							idHabilidad = escanerNumeros.nextInt();
							habilidades = controllerHabilidad.obtenerHabilidadesPorId(idHabilidad);
							if (habilidades != null) {
								controllerHabilidad.borrarHabilidad(idHabilidad);
							} else {
								System.err.println("No existe ninguna habilidad con esa ID");
							}
							break;

						// Volver al menu de Tablas
						case 0:
							System.out.println("Volviendo...");
							Thread.sleep(1000);
							finMenuTablaHabilidades = false;
							break;
						default:
							System.err.println("Opci�n no v�lida");
							break;
						}
					} catch (InterruptedException e) {
						System.err.println("Hilo finalizado antes de tiempo " + e.getMessage());
					}
				}
				break;
			case 0:
				System.out.println("Fin del programa");
				finMenuGlobal = false;
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			default:
				System.err.println("Opci�n no v�lida");
				break;
			}
		}

	}

	/**
	 * 
	 * @param escanerLetras           Recibe como parametro el escaner que se usara
	 *                                para cualquier entrada de teclado del usuario,
	 *                                en este caso usado para letras
	 * @param escanerNumeros          Recibe como parametro el escaner que se usara
	 *                                para cualquier entrada de teclado del usuario,
	 *                                en este caso usado para numeros
	 * @param finMenuVistaHabilidades Recibe como parametro un boolean para poder
	 *                                continuar con el bucle o finalizarlo
	 * @return Devuelve un boolean, true si se han podido mostrar las habilidades,
	 *         false si no se ha podido
	 */
	public static boolean verHabilidades(Scanner escanerLetras, Scanner escanerNumeros,
			boolean finMenuVistaHabilidades) {
		int habilidadPorId;
		while (finMenuVistaHabilidades) {
			System.out.println(
					"\n1.Ver todas las habilidades\n2.Ver una habilidad por id\n3.Ver una habilidad por nombre\n0.Volver");
			int eleccionVistaHabilidades = escanerNumeros.nextInt();

			// Ver todas las habilidades
			if (eleccionVistaHabilidades == 1) {
				if (mostrarHabilidades()) {
					// Se muestra
				} else {
					System.err.println("No se pudieron mostrar las habilidades");
				}

				// Ver una habilidades introduciendo una ID
			} else if (eleccionVistaHabilidades == 2) {
				System.out.println("Introduce la id de la habilidad a mostrar: ");
				habilidadPorId = escanerNumeros.nextInt();
				habilidades = controllerHabilidad.obtenerHabilidadesPorId(habilidadPorId);
				if (habilidades != null) {
					System.out.println(habilidades);
				} else {
					System.err.println("No existe ninguna habilidad con esa id");
				}

				// Ver una habilidad introduciendo su nombre
			} else if (eleccionVistaHabilidades == 3) {
				System.out.println("Introduce el nombre de la habilidad a mostrar: ");
				String nombreHabilidad = escanerLetras.nextLine();
				if (mostrarHabilidadPorNombre(nombreHabilidad)) {
					// Se muestra
				} else {
					System.err.println("No existe una habilidad con ese nombre");
				}

				// Volver al menu de habilidades
			} else if (eleccionVistaHabilidades == 0) {
				finMenuVistaHabilidades = false;
				break;
			}
		}
		return finMenuVistaHabilidades;
	}

	/**
	 * 
	 * @param borrarPersonajePorId Id del personaje a borrar
	 * @return True si se borra. False si no se borra
	 */
	public static boolean borrarPersonaje(int borrarPersonajePorId) {
		boolean seBorra;
		personajes = controllerPersonajes.obtenerPersonajePorId(borrarPersonajePorId);
		if (personajes != null) {
			controllerPersonajes.borrarPersonaje(borrarPersonajePorId);
			seBorra = true;
		} else {
			System.err.println("No existe un personaje con esa ID");
			seBorra = false;
		}
		return seBorra;
	}

	/**
	 * 
	 * @param escanerLetras  Escaner que se usara para cualquier entrada de teclado
	 *                       del usuario, en este caso usado para letras
	 * @param escanerNumeros Escaner que se usara para cualquier entrada de teclado
	 *                       del usuario, en este caso usado para numeros
	 * @param idPersonaje    Id del personaje que se va a querer insertar
	 * @return True si se ha podido insertar un personaje. False si no
	 */
	public static boolean insertarPersonaje(Scanner escanerLetras, Scanner escanerNumeros, int idPersonaje) {
		boolean seInserta;
		String nombrePersonaje;
		int idTipo;
		int idEvento;
		String pasiva;
		personajes = controllerPersonajes.obtenerPersonajePorId(idPersonaje);
		if (personajes != null) {
			System.err.println("Ya existe un personaje con esa ID");
			seInserta = false;

		} else {
			System.out.print("Nombre: ");
			nombrePersonaje = escanerLetras.nextLine();
			System.out.print(
					"Id Tipo: (1.Super PHY\n2.Extreme PHY\n3.Super INT\n4.Extreme INT\n5.Super TEQ\n6.Extreme TEQ\n7.Super AGL"
							+ "\n8.Extreme AGL\n9.Super STR\n10.Extreme STR)");
			idTipo = escanerNumeros.nextInt();
			informacionTipo();
			idEvento = escanerNumeros.nextInt();
			System.out.print("Pasiva personaje:  ");
			pasiva = escanerLetras.nextLine();
			insertarPersonaje(idPersonaje, nombrePersonaje, idTipo, idEvento, pasiva);
			seInserta = true;
		}
		return seInserta;
	}

	/**
	 * 
	 * @param escanerLetras  Escaner que se usara para cualquier entrada de teclado
	 *                       del usuario, en este caso usado para letras
	 * @param escanerNumeros Escaner que se usara para cualquier entrada de teclado
	 *                       del usuario, en este caso usado para numeros
	 * @param idPersonaje    Id del personaje que se quiere actualizar
	 * @return True si se ha podido actualizar el personaje. False si no
	 */
	public static boolean actualizarPersonaje(Scanner escanerLetras, Scanner escanerNumeros, int idPersonaje) {
		boolean seActualiza;
		String nombrePersonaje;
		int idTipo;
		int idEvento;
		String pasiva;
		personajes = controllerPersonajes.obtenerPersonajePorId(idPersonaje);
		if (personajes != null) {
			System.out.print("Nombre: ");
			nombrePersonaje = escanerLetras.nextLine();
			infoTipos();
			idTipo = escanerNumeros.nextInt();
			informacionTipo();
			idEvento = escanerNumeros.nextInt();
			System.out.print("Pasiva personaje:  ");
			pasiva = escanerLetras.nextLine();
			actualizarPersonajes(idPersonaje, nombrePersonaje, idTipo, idEvento, pasiva);
			seActualiza = true;

		} else {
			System.err.println("Error a la hora de actualizar!!");
			seActualiza = false;
		}
		return seActualiza;
	}

	/**
	 * 
	 * @param escanerLetras  Escaner que se usara para cualquier entrada de teclado
	 *                       del usuario, en este caso usado para letras
	 * @param escanerNumeros Escaner que se usara para cualquier entrada de teclado
	 *                       del usuario, en este caso usado para numeros
	 * @param finMenuVista   Boolean que siendo true continua el bucle, y siendo
	 *                       false, finaliza
	 */
	public static void verPersonajes(Scanner escanerLetras, Scanner escanerNumeros, boolean finMenuVista) {
		int eleccionVer;
		int personajePorId;
		int personajePorIdTipo;
		int personajePorIdEvento;
		String personajePorNombre;
		while (finMenuVista) {

			System.out.println(
					"\n1.Ver todos los personajes\n2.Ver un personaje por id\n3.Ver personajes por id tipo\n4.Ver personajes por id evento\n5.Ver un personaje por nombre\n0.Volver");
			eleccionVer = escanerNumeros.nextInt();

			// Ver todos los Personajes
			if (eleccionVer == 1) {
				if (mostrarPersonajes()) {

				} else {
					System.err.println("Error, los personajes no se pudieron mostrar");
				}

				// Ver un personaje dado su ID
			} else if (eleccionVer == 2) {
				System.out.print("Introduce la id del personaje a mostrar:");
				personajePorId = escanerNumeros.nextInt();
				personajes = controllerPersonajes.obtenerPersonajePorId(personajePorId);
				if (personajes != null) {
					System.out.println(personajes);
				} else {
					System.err.println("No existe ningun personaje con esa id");
				}

				// Ver un personaje dado su ID de Tipo
			} else if (eleccionVer == 3) {
				System.out.println("Introduce la id tipo del personaje a mostrar: ");
				personajePorIdTipo = escanerNumeros.nextInt();
				listaPersonajes = (ArrayList<Personaje>) controllerPersonajes
						.obtenerPersonajesPoridTipo(personajePorIdTipo);

				if (!listaPersonajes.isEmpty()) {
					for (Personaje personaje : listaPersonajes) {
						System.out.println(personaje);
					}
				} else {
					System.err.println("No hay ningun personaje con esa id de tipo");
				}

				// Ver un personaje dado su Id de Evento
			} else if (eleccionVer == 4) {
				System.out.println("Introduce la id evento del personaje a mostrar: ");
				personajePorIdEvento = escanerNumeros.nextInt();
				listaPersonajes = (ArrayList<Personaje>) controllerPersonajes
						.obtenerPersonajesPorIdEvento(personajePorIdEvento);
				if (!listaPersonajes.isEmpty()) {
					for (Personaje personaje : listaPersonajes) {
						System.out.println(personaje);
					}
				} else {
					System.err.println("No hay ningun personaje con esa id de evento");
				}

				// Ver un personaje dado su Nombre
			} else if (eleccionVer == 5) {
				System.out.println("Introduce el nombre del personaje a mostrar: ");
				personajePorNombre = escanerLetras.nextLine();
				listaPersonajes = (ArrayList<Personaje>) controllerPersonajes
						.obtenerPersonajesPorNombre(personajePorNombre);
				if (!listaPersonajes.isEmpty()) {
					for (Personaje personaje : listaPersonajes) {
						System.out.println(personaje);
					}
				} else {
					System.err.println("No hay ningun personaje con ese nombre");
				}

				// Volver al menu Personaje
			} else if (eleccionVer == 0) {
				finMenuVista = false;
				break;
			}

		}
	}

	/**
	 * Metodo que permite ver los tipos de los personajes
	 */
	public static void infoTipos() {
		System.out.println("Id Tipo:"
				+ " \n1.Super PHY\n2.Extreme PHY\n3.Super INT\n4.Extreme INT\n5.Super TEQ\n6.Extreme TEQ\n7.Super AGL"
				+ "\n8.Extreme AGL\n9.Super STR\n10.Extreme STR");
	}

	/**
	 * Metodo que permite ver la informacion de los tipos de evento disponibles para
	 * los personajes
	 */
	public static void informacionTipo() {
		System.out.println(
				"Id Evento: \n1.Warrior of fury! Super Saiyan\n2.Young Warrior's Resolve\n3.Unified as a Super Namekian"
						+ "\n4.Earth-Shaking Showdown(vs. Goku)\n5.Surpassing Even the Gods\n6.Earth-Shaking Showdown(vs. Vegeta)"
						+ "\n7.Proud Prince of the Saiyans\n8.Beacon of Hope in the War-Stricken Future\n9.Warrior Returned from Otherworld"
						+ "\n10.Defender of Beliefs and Dreams\n11.Sinister Destroyer of the Universe\n12.Earth-Shaking Showdown (vs. Frieza)"
						+ "\n13.Ultimate Android, Incarnation of Ambitions\n14.Phantom Majin of Smoke and Flame\n15.Battle-Hardened Saiyan Power Explodes!"
						+ "\n16.Super Awakening in the Gap of History\n17.Battle-Hardened Saiyan Power Explodes!\n18.The invincible\n19.Split-Second Assasin"
						+ "\n20.The Greatest Adversary of all\n21.The Darnkness Shrouding\n22.Penetrate! Full-Throttle Throwdown\n23.Ultimate Android");
	}

	/**
	 * Metodo que permite mostrar los personajes, almacenandolo en un
	 * ArrayList<Personaje>
	 * 
	 * @return True si se han podido mostrar los personajes. False si no se han
	 *         podido mostrar los personajes.
	 */
	public static boolean mostrarPersonajes() {
		boolean seMuestraPersonaje;
		ArrayList<Personaje> lista = new ArrayList<Personaje>();
		lista = (ArrayList<Personaje>) controllerPersonajes.obtenerPersonajes();
		if (!lista.isEmpty()) {
			for (Personaje personaje : lista) {
				System.out.println(personaje);
			}
			seMuestraPersonaje = true;
		} else {
			seMuestraPersonaje = false;
		}

		return seMuestraPersonaje;
	}

	/**
	 * Metodo que permite actualizar un Personaje dada su ID
	 * 
	 * @param idPersonaje     Entrada de usuario, si la ID no existe, no se podra
	 *                        actualizar en la base de datos
	 * @param nombrePersonaje Entrada de usuario
	 * @param idTipo          Entrada de usuario
	 * @param idEvento        Entrada de usuario
	 * @param pasivaPersonaje Entrada de usuario
	 */
	public static void actualizarPersonajes(int idPersonaje, String nombrePersonaje, int idTipo, int idEvento,
			String pasivaPersonaje) {

		Personaje nuevoPersonaje = new Personaje(idPersonaje, nombrePersonaje, idTipo, idEvento, pasivaPersonaje);
		controllerPersonajes.actualizarPersonaje(nuevoPersonaje, idPersonaje);
	}

	/**
	 * Metodo que permite insertar un personaje
	 * 
	 * @param idPersonaje     Entrada de usuario, si la ID ya existe, no se podra
	 *                        insertar un personaje a la base de datos
	 * @param nombrePersonaje Entrada de usuario
	 * @param idTipo          Entrada de usuario
	 * @param idEvento        Entrada de usuario
	 * @param pasivaPersonaje Entrada de usuario
	 */
	public static void insertarPersonaje(int idPersonaje, String nombrePersonaje, int idTipo, int idEvento,
			String pasivaPersonaje) {
		controllerPersonajes.insertarPersonaje(idPersonaje, nombrePersonaje, idTipo, idEvento, pasivaPersonaje);
	}

	/**
	 * Metodo que nos permite recoger en un ArrayList<Habilidad> las habilidades
	 * disponibles en nuestra base de datos
	 * 
	 * @return True si se muestran las Habilidades. False si no.
	 */
	public static boolean mostrarHabilidades() {
		boolean seMuestraHabilidad;
		ArrayList<Habilidad> lista = new ArrayList<Habilidad>();
		lista = (ArrayList<Habilidad>) controllerHabilidad.obtenerHabilidades();
		if (!lista.isEmpty()) {
			for (Habilidad habilidad : lista) {
				System.out.println(habilidad);
			}
			seMuestraHabilidad = true;
		} else {
			seMuestraHabilidad = false;
		}
		return seMuestraHabilidad;
	}

	/**
	 * Metodo que nos permite recoger en un ArrayList<Habilidad> las habilidades
	 * disponibles en nuestra base de datos
	 * 
	 * @param nombreHabilidad Nombre de la habilidad a mostrar
	 * @return True si se muestra la habilidad. False si no
	 */
	public static boolean mostrarHabilidadPorNombre(String nombreHabilidad) {
		boolean seMuestraHabilidad;
		ArrayList<Habilidad> lista = new ArrayList<Habilidad>();
		lista = (ArrayList<Habilidad>) controllerHabilidad.obtenerHabilidadPorNombre(nombreHabilidad);
		if (!lista.isEmpty()) {
			for (Habilidad habilidad : lista) {
				System.out.println(habilidad);
			}
			seMuestraHabilidad = true;
		} else {
			seMuestraHabilidad = false;
		}
		return seMuestraHabilidad;
	}

	/**
	 * 
	 * @param escanerLetras Escaner que se usara para cualquier entrada de teclado
	 *                      del usuario, en este caso usado para letras
	 * @param idHabilidad   Id de la habilidad a actualizar
	 * @return True si se ha podido actualizar la habilidad. False si no
	 */
	public static boolean actualizarHabilidad(Scanner escanerLetras, int idHabilidad) {
		boolean seActualiza;
		String nombreHabilidad;
		String efectoHabilidad;
		habilidades = controllerHabilidad.obtenerHabilidadesPorId(idHabilidad);
		if (habilidades != null) {
			System.out.println("Nombre habilidad: ");
			nombreHabilidad = escanerLetras.nextLine();
			System.out.println("Efecto habilidad: ");
			efectoHabilidad = escanerLetras.nextLine();
			Habilidad nuevaHabilidad = new Habilidad(idHabilidad, nombreHabilidad, efectoHabilidad);
			controllerHabilidad.actualizarHabilidad(nuevaHabilidad, idHabilidad);
			seActualiza = true;

		} else {
			System.err.println("No se pudo actualizar la habilidad");
			seActualiza = false;
		}
		return seActualiza;
	}

	/**
	 * 
	 * @param idHabilidad     Id de la habilidad a insertar
	 * @param nombreHabilidad Nombre de la habilidad a insertar
	 * @param efectoHabilidad Efecto de la habilidad a insertar
	 */
	public static void insertarHabilidad(int idHabilidad, String nombreHabilidad, String efectoHabilidad) {
		controllerHabilidad.insertarHabilidad(idHabilidad, nombreHabilidad, efectoHabilidad);
	}

}
