package modelo;

public class Personaje {
	private int idPersonaje;
	private String nombrePersonaje;
	private int idTipo;
	private int idEvento;
	private String pasivaPersonaje;

	public Personaje(int idPersonaje, String nombrePersonaje, int idTipo, int idEvento, String pasivaPersonaje) {
		this.idPersonaje = idPersonaje;
		this.nombrePersonaje = nombrePersonaje;
		this.idTipo = idTipo;
		this.idEvento = idEvento;
		this.pasivaPersonaje = pasivaPersonaje;
	}

	public int getIdPersonaje() {
		return idPersonaje;
	}

	public void setIdPersonaje(int idPersonaje) {
		this.idPersonaje = idPersonaje;
	}

	public String getNombrePersonaje() {
		return nombrePersonaje;
	}

	public void setNombrePersonaje(String nombrePersonaje) {
		this.nombrePersonaje = nombrePersonaje;
	}

	public int getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}

	public int getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}

	public String getPasivaPersonaje() {
		return pasivaPersonaje;
	}

	public void setPasivaPersonaje(String pasivaPersonaje) {
		this.pasivaPersonaje = pasivaPersonaje;
	}

	@Override
	public String toString() {
		return "\n----------------------" + "\nId Personaje: " + idPersonaje + "\n----------------------"
				+ "\nNombre personaje: " + nombrePersonaje + "\n----------------------" + "\nId Tipo: " + idTipo
				+ "\n---------------------- " + "\nId Evento: " + idEvento + "\n----------------------" + "\nPasiva personaje: " + pasivaPersonaje +
				"\n---------------------------------------------------------" + "\n";
	}
}
