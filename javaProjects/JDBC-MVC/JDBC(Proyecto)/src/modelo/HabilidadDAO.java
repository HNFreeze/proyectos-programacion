package modelo;

import java.util.List;

public interface HabilidadDAO {
	public List<Habilidad> obtenerHabilidades();
	public List<Habilidad> obtenerHabilidadPorNombre(String nombreHabilidad);
	public Habilidad obtenerHabilidadesPorId(int idHabilidad);
	public boolean insertarHabilidad(int idHabilidad, String nombreHabilidad, String efectoHabilidad);
	public boolean borrarHabilidad(int idHabilidad);
	public boolean actualizarHabilidad(Habilidad nuevaHabilidad,int id_a_Actualizar);
}
