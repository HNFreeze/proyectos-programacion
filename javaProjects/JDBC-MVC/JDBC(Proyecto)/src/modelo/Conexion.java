package modelo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class Conexion {
	private String driver;
	private String dbms;
	private int numeroPuerto;
	private String nombreServidor;
	private String dbName;
	private String user;
	private String pass;
	private Properties prop;
	/**
	 * Constructor que nos permite recoger la ruta de los argumentos de ejecucion, y vincularlo.
	 * @param archivoProperties Recibe la ruta del xml desde los args[0], con la informacion de la base de datos
	 * @throws InvalidPropertiesFormatException Lanza una excepcion si el formato del xml no es valido
	 * @throws IOException Lanza una excepcion de entrada/salida
	 */
	public Conexion(String archivoProperties) throws InvalidPropertiesFormatException, IOException {
	this.setProperties(archivoProperties);
	}
	/**
	 * Metodo que nos permite sincronizar los datos de nuestra base de datos gracias a un xml con un formato determinado
	 * @param nombreArchivo Recibe como parametro la variable del constructor de la clase Conexion 
	 * @throws InvalidPropertiesFormatException Lanza una excepcion si el formato del xml no es valido
	 * @throws IOException Lanza una excepcion de entrada/salida
	 */
	private void setProperties(String nombreArchivo) throws InvalidPropertiesFormatException, IOException {
		this.prop = new Properties();
		prop.loadFromXML(Files.newInputStream(Paths.get(nombreArchivo)));

		this.dbms = this.prop.getProperty("dbms");
		this.driver = this.prop.getProperty("driver");
		this.dbName = this.prop.getProperty("database_name");
		this.user = this.prop.getProperty("user_name");
		this.pass = this.prop.getProperty("password");
		this.nombreServidor = this.prop.getProperty("server_name");
		this.numeroPuerto = Integer.parseInt(this.prop.getProperty("port_number"));
	}
	/**
	 * Metodo que nos permite realizar una conexion con la base de datos, recogiendo los datos de un xml con un formato determinado
	 * @return Devuelve una variable de la clase Conection con informacion de si se ha creado la base de datos o no
	 * @throws SQLException Lanza una excepcion si ocurre algun problema a la hora de intentar conectarse a la base de datos
	 */
	public Connection getConnection() throws SQLException {
		Connection con = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", this.user);
		connectionProps.put("password", this.pass);
		if (this.dbms.equals("mysql")) {
			con = DriverManager.getConnection(
					"jdbc:" + this.dbms + "://" + this.nombreServidor + ":" + this.numeroPuerto + "/" + this.dbName,
					connectionProps);
		} else if (this.dbms.equals("derby")) {
			con = DriverManager.getConnection("jdbc:" + this.dbms + ":" + this.dbName + ";create=true",
					connectionProps);
		}
		System.out.println("Conectado a la Base de Datos del Dokkan Battle!!");
		return con;
	}
	/**
	 * Metodo que nos permite cerrar la conexion de nuestra base de datos
	 * @param conexion Recibe como parametro un objeto de la clase Connection
	 * @throws SQLException Lanza una excepcion si ocurre algun problema a la hora de intentar conectarse a la base de datos
	 **/
	public void closeConnection(Connection conexion) throws SQLException {
		conexion.close();
	}

}
