package modelo;

import java.util.List;

public interface PersonajeDAO {
	public List<Personaje> obtenerPersonajes();
	public List<Personaje> obtenerPersonajesPorNombre(String nombrePersonaje);
	public List<Personaje> obtenerPersonajesPoridTipo(int idTipoPersonaje);
	public List<Personaje> obtenerPersonajesPorIdEvento(int idEventoPersonaje);
	public Personaje obtenerPersonajePorId(int id);
	public boolean insertarPersonaje(int idPersonaje, String nombre, int idTipo, int idEvento, String pasiva);
	public boolean borrarPersonaje(int id);
	public boolean actualizarPersonaje(Personaje nuevoPersonaje,int id_a_Actualizar);
}
