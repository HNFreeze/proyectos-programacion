package modelo;

public class Habilidad {
	private int idHabilidad;
	private String nombreHabilidad;
	private String efectoHabilidad;

	public int getIdHabilidad() {
		return idHabilidad;
	}

	public void setIdHabilidad(int idHabilidad) {
		this.idHabilidad = idHabilidad;
	}

	public String getNombreHabilidad() {
		return nombreHabilidad;
	}

	public void setNombreHabilidad(String nombreHabilidad) {
		this.nombreHabilidad = nombreHabilidad;
	}

	public String getEfectoHabilidad() {
		return efectoHabilidad;
	}

	public void setEfectoHabilidad(String efectoHabilidad) {
		this.efectoHabilidad = efectoHabilidad;
	}

	public Habilidad(int idHabilidad, String nombreHabilidad, String efectoHabilidad) {
		this.idHabilidad = idHabilidad;
		this.nombreHabilidad = nombreHabilidad;
		this.efectoHabilidad = efectoHabilidad;
	}

	@Override
	public String toString() {
		return "\n----------------------" + "\nId Habilidad: " + idHabilidad + "\n----------------------"
				+ "\nNombre habilidad: " + nombreHabilidad + "\n----------------------" + "\nEfecto habilidad: "
				+ efectoHabilidad + "\n----------------------";
	}

}
